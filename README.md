## Description
PINT (Python INTegrated tokamak model) is a minimal integrated modelling suite in Python for tokamak 1D core transport. 

The code was originally developed for educational purposes for a TU/e Fusion masterclass. 

## Technical Details
The model presently calculates current diffusion, heat and particle transport in either ad-hoc circular or shaped geometry based on CHEASE equilibria. 

Turbulent transport coefficients can be chosen between constant values, a Critical Gradient Model for ITG based on the Guo Romanelli 1993 formula, and the QuaLiKiz-neural-network (van de Plassche PoP 2020). Parameterized heat and current sources are user-set. Default parameters are based on ITER. The PDE solution relies on a linear implicit solver using the FiPy library, with the Pereverzev method ensuring solution stability at large timesteps in spite of the stiffness and nonlinearity. Internal boundary conditions are set with an implicit source method. 

Stationary state on confinement timescales is typically reached in under 1 minute of wall time. Every timestep can be visualized, with user-choices on the degree of interactive visualization detail. In addition, outputs with time history can be saved as h5 files.

The code is run by setting up yaml configuration files, which modify the default parameters specified in `parameters_template.py`. Run `main.py -h` for a list of code input arguments related to config file paths and output file paths. Post-simulation plotting is carried out with the `plot.py` tool. Run `plot.py -h` for more details. The `main.py` simulation driver file is in the `src/pintmod` directory.

## Installation
The [FiPy](https://www.ctcms.nist.gov/fipy) library is used, and can be installed with pip, e.g. ```pip install --user fipy```. Other packages that are necessary to install are found in `requirements.txt`. For plotting, it may be necessary (depending on your system) to install a GUI backend like pyqt5. 

For usage of the QuaLiKiz-neural-network (QLKNN), the QLKNN-develop package must be installed and the QLKNN-hyper-10D model weights and biases downloaded.

1. Clone the PINT repository, e.g. ```git clone git@gitlab.com:qualikiz-group/pyntegrated_model.git pint```
2. Clone the [QLKNN-develop](https://gitlab.com/qualikiz-group/QLKNN-develop) repository, e.g. run ```git clone git@gitlab.com:qualikiz-group/QLKNN-develop qlknn``` (but not from inside the PINT repository)
3. Install the qlknn package. i.e. go to the main directory of the cloned QLKNN-repository repository, and run ```pip install --user .```
4. Download the weights and biases of the QLKNN-hyper-10 network. Recommended to do so from within the QLKNN-develop repository, e.g. from within the `QLKNN-develop/qlknn/models` directory, run `git clone git@gitlab.com:qualikiz-group/qlknn-hyper.git`. This downloads the QLKNN-hyper-10D neural network weights and biases in JSON format to a directory names qlknn-hyper (see K.L. van de Plassche, PoP 2020) 

Before running the model, be sure to ascertain that the path of the qlknn model in `parameters_template.py` (in the PINT repository) is the path of the just-downloaded NN model JSON files on your system. No changes should be necessary if the qlknn package and NN model were installed as according to steps (2) and (3) above. 

For running with the MUSCLE3 library, switch to the ```feature/muscle3_updated``` branch. Ensure that the MUSCLE3 Python library is installed with e.g. ```pip install --user muscle3```

## How to run

Without MUSCLE:

1. If not yet existing,  recommmended to create a `runs` directory from within the main PINT directory and/or the `src/pintmod` directory
2. In general, the API is ```python3 $HOME/$PINTDIR/src/pintmod/main.py --config CONFIGFILE --outfile OUTPUTFILE```. Where CONFIGFILE is relative path to the configuration yaml file, which overwrites any configuration defaults which are listed in parameter_template.py. The OUTPUTFILE is relative path to the output h5 file, which can be used for visualization later.

For example, to run one of the test cases, e.g. with the QuaLiKiz-neural-network, can run from the PINT `src/pintmod` directory: ```python3 main.py --config tests/data/test7 --outfile runs/test7out```. This will read the ```config_tests/test7.yaml``` configuration file and output the results to ```runs/test7out.h5```. 

3. To visualize the results, use the plot.py tool. The API is: ```python3 plot.py --outfile OUTFILE1 OUTFILE2(optional) --chimax CHIMAX```. OUTFILE1 is mandatory, and is the name of the h5 output file to visualize. OUTFILE2 is optional and if existing, will be plotted in a comparative way to OUTFILE1. CHIMAX provides the maximum value of the heat conductivity in the plots. For the example above, can test with ```python3 plot.py --outfile runs/test7out --chimax 5```. The visualization tool has a slider that allows exploration of the entire run evolution.


With MUSCLE:

Similar to above, except that a bash script is used. The bash script API is ```./pint_exec.sh CONFIGFILE OUTPUTFILE```, with CONFIGFILE and OUTPUTFILE as above. The bash script sets up the workflow ymmsl file, launches the Muscle Manager, and the PDE + transport model implementations (both as Python processes). The bash script prints to stdout the progress of the PDE implementation, resulting in a similar UX to the non-MUSCLE version. The example from above is run from the root pint directory as ```./pint_exec.sh src/pintmod/tests/data/test7 runs/test7out```

## Usage
Primarily for educational purposes within the framework of a Technical University of Eindhoven Masterclass in gyrokinetics and integrated modelling. Also useful for rapid scenario prototyping and intuition building, as a precursor to applying more complete integrated models. 

## Contributing
Open for contributions.

## Authors 
[Jonathan Citrin](mailto:J.Citrin@differ.nl)

## Acknowledgements
Karel van de Plassche for the QLKNN model and associated Python library 

## License
MIT

## Project status
Non-continuous development. See issues for a list of desired enhancements


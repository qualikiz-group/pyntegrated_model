class Input:
    import os

    # geometry setting
    geometry_type = 'circular' # choose 'circular' or 'CHEASE'. 'circular' is for quick testing purposes and not recommended
    geometry_file = 'ITER_hybrid_citrin_equil_cheasedata.mat2cols'

    Ip_from_parameters = True # take Ip from parameter file if true and and rescale psi. Otherwise from CHEASE
    test_geometry = False # if true, plot comparisons of various geometry variables and exit simulation

    # physical inputs
    Rmaj = 6.2 # major radius (R) in meters
    Rmin = 2.0 # minor radius (a) in meters
    kappa = 1.72 # ITER LCFS elongation, to approximately correct volume and area integral Jacobians
    Ai = 2.5 # amu of main ion (if multiple isotope, make average)
    Ip = 15 # total plasma current in MA 
    B = 5.3 # Toroidal magnetic field (on axis) [T]
    Zeff = 1 # needed for qlknn and fusion power
    Zi = 1 # main ion charge
    Zimp = 10 # impurity charge state assumed for dilution

    # boundary + initial conditions for T and n
    T_bound_left = 15 # initial condition temperature for r=0
    T_bound_right = 1 # boundary condition temperature for r=Rmin

    ne_bound_right = 0.5 #boundary condition density for r=Rmin
    nbar = 1 # line averaged density (not used if f_GW = True), in units of reference density (nref)
    set_fGW = True # set initial condition density according to Greenwald fraction. Otherwise from nbar
    fGW = 0.85 # Initial condition Greenwald fraction (nGW = Ip/(pi*a^2)) with a in m, nGW in 10^20 m-3, Ip in MA
    npeak = 1.5 # Peaking factor of density profile

    # external heat source parameters
    w = 0.5 # Gaussian width
    rsource = 0.0 # Source Gaussian central location
    Ptot = 120e6 # total heating
    el_heat_ratio = 0.66666 # electron heating ratio
    ion_heat_ratio = 1 - el_heat_ratio  # ion heating ratio
    Qei_mult = 1 # multiplier for ion-electron heat exchange term for sensitivity testing
    calculate_Pfus = True # calculate fusion power
    use_fusion_heat_source = True # incorporate fusion heating source in calculation

    # particle source parameters
    pellet_width = 0.1 # Gaussian width of pellet deposition (normalized radial coordinate) in continuous pellet model
    pellet_deposition_location = 0.85 # Pellet source Gaussian central location (normalized radial coordinate) in continuous pellet model
    S_pellet_tot = 2e22 # total pellet particles/s (continuous pellet model)

    puff_decay_length = 0.05 # exponential decay length of gas puff ionization (normalized radial coordinate)
    S_puff_tot = 1e22 # total gas puff particles/s

    nbi_particle_width = 0.25 # NBI particle source Gaussian width (normalized radial coordinate)
    nbi_deposition_location = 0.0 # NBI particle source Gaussian central location (normalized radial coordinate)
    S_nbi_tot = 1e22 # NBI total particle source

    # current profiles (broad "Ohmic" + localized "external" currents)
    nu = 3 # peaking factor of "Ohmic" current: johm = j0*(1 - r^2/a^2)^nu
    fext = 0.2 # total "external" current fraction
    wext = 0.05 # width of "external" Gaussian current profile (normalized radial coordinate)
    rext = 0.4 # radius of "external" Gaussian current profile (normalized radial coordinate)
    q_correction_factor = 1.38 # correction factor such that q95 = 3 for standard ITER parameters (ad-hoc fix for non-physical circular geometry model)
    resistivity_mult = 100 # 1/multiplication factor for sigma (conductivity) to reduce current diffusion timescale to be closer to heat diffusion timescale
    bootstrap_mult = 1 # Multiplication factor for bootstrap current

    # numerical (e.g. no. of grid points, other info needed by solver)
    nr = 25 # radial grid points
    hires_fac = 4 # grid refinement factor for poloidal flux <--> plasma current calculations
    maxdt = 1e-1  #
    dtmult = 0.9*10 # multiplier in front of the base timestep dt=dx^2/(2*chi). Can likely be increased further beyond this default
    use_pereverzev_in_heat = True # use Pereverzev rule in heat transport equation
    use_pereverzev_in_dens = True # use Pereverzev rule in density transport equation
    chi_per = 20 # (deliberately) large heat conductivity for Pereverzev rule
    D_per = 10 # (deliberately) large particle diffusion for Pereverzev rule
    implicit = True # if use_pereverzev = True then implicit = True regardless of user setting
    heat_eq = True
    current_eq = False
    dens_eq = False

    # plotting
    show_Pfus = True # show fusion power in plots
    show_fGW = True # show Greenwald fraction in plots
    Tplot_ymax = 40 # upper limit of y-axis in the T-profile plots
    chi_ylim = 3 # upper limit in chi plot
    Qlowlim = -0.5e3 #lower limit in source plot
    Qhilim = 1.5e3 #upper limit in source plot
    plot_T = True # plots evolution of temperature. If false, plotchi and plotall have no impact (nothing is plotted)
    plotchi = True #Plots chii, chiee extra outputs. Slows down the simulation.
    plotall = True  #Plots all extra outputs. Warning: slows down the simulation even more! If True, overwrites plotchi
    write_dt = False #writes timestep to stdout

    # allowed chi and diffusivity bounds
    chimin = 0.05 # minimum chi
    chimax = 100 # maximum chi (can be helpful for stability)
    Demin = 0.05 # minimum electron diffusivity

    # set inner core transport coefficients (ad-hoc MHD/EM transport)
    apply_inner_patch = False
    De_inner = 0.2
    Ve_inner = 0.0
    chii_inner = 1
    chie_inner = 1 
    rho_inner = 0.3 # radius below which patch transport is applied

    # set outer core transport coefficients (e.g. for L-mode)
    apply_outer_patch = False
    De_outer = 0.2
    Ve_outer = 0.0 
    chii_outer = 1
    chie_outer = 1 
    rho_outer = 0.9 # radius above which patch transport is applied

    # for CGM model
    CGMalpha = 2 # exponent of chi power law: chi \propto (R/LTi - R/LTi_crit)^alpha
    CGMchistiff = 2 # stiffness parameter
    CGMchiei_ratio = 2 # ratio of electron to ion transport coefficient (ion higher: ITG)
    CGM_D_ratio = 5 # ratio of ion heat conductivity to electron diffusivity

    # for QLKNN model
    model_path = os.getenv('HOME')+'/qlknn/qlknn/models/qlknn-hyper' # path of weights and biases (user-dependent)
    coll_mult = 0.25 # collisionality multiplier in QLKNN for sensitivity testing. Default is 0.25 (correction factor to a more recent QLK collision operator)
    avoid_big_negative_s = True # ensure that smag - alpha > -0.2 always, to compensate for no slab modes
    include_ITG = True # to toggle ITG modes on or off
    include_TEM = True # to toggle ITG modes on or off
    include_ETG = True # to toggle ITG modes on or off
    ITG_flux_ratio_correction = 2 # The QLK version this specific QLKNN was trained on tends to underpredict ITG electron heat flux in shaped, high-beta scenarios. This is a correction factor
    DVeff = False # effective D / effective V approach for particle transport
    An_min = 0.05 # minimum |R/Lne| below which effective V is used instead of effective D

    # for constant chi model
    chii = 1 # coefficient in ion heat equation diffusion term in m^2/s
    chie = 1 # coefficient in electron heat equation diffusion term in m^2/s
    De = 1 # diffusion coefficient in electron density equation in m^2/s
    Ve = -0.33 # convection coefficient in electron density equation in m^2/s

    # simulation control
    finaltime = 5 # length of simulation time in seconds
    transport_model = 'qlknn' # 'constant', 'CGM', or 'qlknn'

    # internal boundary condition (pedestal)
    set_pedestal = True # do not set internal boundary condition if this is False
    Tiped = 5 # ion pedestal top temperature in keV for Ti and Te
    Teped = 5 # electron pedestal top temperature in keV for Ti and Te
    neped = 0.7 # pedestal top electron density in units of nref
    Ped_top = 0.91  # set ped top location in normalized radius
    largeValue_T = 1e10 # effective source to dominate PDE in internal boundary condtion location if T != Tped
    largeValue_n = 1e8 # effective source to dominate density PDE in internal boundary condtion location if n != neped

    # reference values for normalizations
    nref = 1e20

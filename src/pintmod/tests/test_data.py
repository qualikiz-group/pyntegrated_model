import os
from pathlib import Path, PosixPath

import pytest
import h5py
import numpy as np

from .conftest import data_dir
from IPython import embed


def data_file(testnum, tmpdir):
    atol = 1e-11
    test_dir: PosixPath = (data_dir / "..").resolve()
    os.chdir(test_dir)
    config = data_dir / f"test{testnum}"
    outfile = tmpdir / f"tmp{testnum}"
    os.system(
        f"python3 ../main.py --config {config} --outfile {outfile}"
    )


def runtest(testnum, tmpdir, atol=1e-11):
    old_wd = Path.cwd()
    # Read reference file
    h5_path = data_dir / Path(f"test{testnum}.h5")
    with h5py.File(h5_path, "r") as hf_ref:
        Ti_ref = hf_ref["Ti"][:]
        Te_ref = hf_ref["Te"][:]
        ne_ref = hf_ref["ne"][:]
        psi_ref = hf_ref['psi'][:]
        q_ref = hf_ref['q'][:]
        s_ref = hf_ref['s'][:]
        t_ref = hf_ref["t"][:]

    # Create and read new file
    data_file(testnum, tmpdir)
    os.chdir(tmpdir)
    new_h5_path: py._path.local.LocalPath = tmpdir / Path(f"tmp{testnum}.h5")
    assert new_h5_path.exists(), f"{new_h5_path} not created by data_file!"

    # Read created file
    with h5py.File(new_h5_path, "r") as hf_new:
        Ti_new = hf_new["Ti"][:]
        Te_new = hf_new["Te"][:]
        ne_new = hf_new["ne"][:]
        psi_new = hf_new['psi'][:]
        q_new = hf_new['q'][:]
        s_new = hf_new['s'][:]
        t_new = hf_new["t"][:]

    # Test within tolerance
    assert np.all(abs(t_new - t_ref) < atol)
    assert np.all(abs(Te_new - Te_ref) < atol)
    assert np.all(abs(Ti_new - Ti_ref) < atol)
    assert np.all(abs(ne_new - ne_ref) < atol)
    assert np.all(abs(psi_new - psi_ref) < atol)
    assert np.all(abs(q_new - q_ref) < atol)
    assert np.all(abs(s_new - s_ref) < atol)


# explicit, Ti only, no Pei, no pedestal, constant chi
def test_1(tmpdir):
    runtest(1, tmpdir)


# implicit, Ti+Te, no Pei, no pedestal, constant chi
def test_2(tmpdir):
    runtest(2, tmpdir)


# test3: implicit, Ti+Te, Pei low dens, no pedestal, constant chi
def test_3(tmpdir):
    runtest(3, tmpdir)


# test4: implicit, Ti+Te, Pei high dens, no pedestal, constant chi
def test_4(tmpdir):
    runtest(4, tmpdir)


# test5: implicit, Ti+Te, Pei standard dens, pedestal, constant chi
def test_5(tmpdir):
    runtest(5, tmpdir)


# test6: implicit + pereverzev-corrigan, Ti+Te, Pei standard dens, pedestal, chi from CGM
def test_6(tmpdir):
    runtest(6, tmpdir)


# test7: implicit + pereverzev-corrigan, Ti+Te, Pei standard dens, pedestal, chi from QLKNN
def test_7(tmpdir):
    runtest(7, tmpdir)


# test8: implicit. Current diffusion only
def test_8(tmpdir):
    runtest(8, tmpdir)


# test9: implicit + pereverzev-corrigan, Ti+Te+Psi, Pei standard dens, pedestal, chi from QLKNN
def test_9(tmpdir):
    runtest(9, tmpdir)

# test10: implicit + pereverzev-corrigan, Ti+Te+Psi, Pei standard dens, pedestal, chiCGM, bootstrap current
def test_10(tmpdir):
    runtest(10, tmpdir)

# test11: current, heat, and particle transport. Constant transport coefficient model
def test_11(tmpdir):
    runtest(11, tmpdir)

# test12: current, heat, and particle transport. Constant transport coefficient model. Pedestal
def test_12(tmpdir):
    runtest(12, tmpdir)

# test13: current, heat, and particle transport. Constant transport coefficient model. Pedestal. Particle sources
def test_13(tmpdir):
    runtest(13, tmpdir)

# test14: current, heat, and particle transport. CGM transport model. Pedestal. Particle sources
def test_14(tmpdir):
    runtest(14, tmpdir)

# test15: current, heat, and particle transport. CGM transport model. Pedestal. Particle sources including NBI
def test_15(tmpdir):
    runtest(15, tmpdir)

# test16: current, heat, and particle transport. qlknn transport model. Pedestal. Particle sources including NBI. PC method for density. D_e scaled from chi_e
def test_16(tmpdir):
    runtest(16, tmpdir)

# test17: current, heat, and particle transport. qlknn transport model. Pedestal. Particle sources including NBI. PC method for density. Deff + Veff model.
def test_17(tmpdir):
    runtest(17, tmpdir)

# test18: current, heat, and particle transport. CGM transport model. Pedestal. Particle sources including NBI. Self-consistent fusion power.
def test_18(tmpdir):
    runtest(18, tmpdir)

# test19: current, heat, and particle transport. qlknn transport model. Pedestal. Particle sources including NBI. PC method for density. D_e scaled from chi_e. Self-consistent fusion power.
def test_19(tmpdir):
    runtest(19, tmpdir)

# test20: CHEASE geometry. Ip from CHEASE. explicit, Ti only, no Pei, no pedestal, constant chi
def test_20(tmpdir):
    runtest(20, tmpdir)

# test21: CHEASE geometry. Ip from parameters. implicit, Ti+Te, no Pei, no pedestal, constant chi. General geometry
def test_21(tmpdir):
    runtest(21, tmpdir)

# test22: CHEASE geometry. Ip from parameters. implicit, Ti+Te, Pei low dens, no pedestal, constant chi
def test_22(tmpdir):
    runtest(22, tmpdir)

# test23: CHEASE geometry. Ip from parameters. implicit, Ti+Te, Pei high dens, no pedestal, constant chi
def test_23(tmpdir):
    runtest(23, tmpdir)

# test24: CHEASE geometry. Ip from parameters. implicit, Ti+Te, Pei standard dens, pedestal, constant chi
def test_24(tmpdir):
    runtest(24, tmpdir)

# test25: CHEASE geometry. Ip from parameters. implicit + pereverzev-corrigan, Ti+Te, Pei standard dens, pedestal, chi from CGM
def test_25(tmpdir):
    runtest(25, tmpdir)

# test26: CHEASE geometry. Ip from parameters. implicit + pereverzev-corrigan, Ti+Te, Pei standard dens, pedestal, chi from QLKNN
def test_26(tmpdir):
    runtest(26, tmpdir)

# test27: CHEASE geometry. Ip from parameters. implicit, psi (current diffusion) only
def test_27(tmpdir):
    runtest(27, tmpdir)

# test28: CHEASE geometry. Ip from CHEASE. implicit, psi (current diffusion) only
def test_28(tmpdir):
    runtest(28, tmpdir)

# test29: CHEASE geometry. Ip from parameters. implicit + pereverzev-corrigan, Ti+Te+Psi, Pei standard dens, pedestal, chi from QLKNN
def test_29(tmpdir):
    runtest(29, tmpdir)

# test30: CHEASE geometry. Ip from parameters. implicit + pereverzev-corrigan, Ti+Te+Psi, Pei standard dens, pedestal, chiCGM, bootstrap current
def test_30(tmpdir):
    runtest(30, tmpdir)

# test31: CHEASE geometry. Ip from parameters. current, heat, and particle transport. Constant transport coefficient model
def test_31(tmpdir):
    runtest(31, tmpdir)

# test32: CHEASE geometry. Ip from parameters. current, heat, and particle transport. Constant transport coefficient model. Pedestal
def test_32(tmpdir):
    runtest(32, tmpdir)

# test33: CHEASE geometry. Ip from parameters. current, heat, and particle transport. Constant transport coefficient model. Pedestal. Particle sources
def test_33(tmpdir):
    runtest(33, tmpdir)

# test34: CHEASE geometry. Ip from parameters. current, heat, and particle transport. CGM transport model. Pedestal. Particle sources
def test_34(tmpdir):
    runtest(34, tmpdir)

# test35: CHEASE geometry. Ip from parameters. current, heat, and particle transport. CGM transport model. Pedestal. Particle sources including NBI
def test_35(tmpdir):
    runtest(35, tmpdir)

# test36: CHEASE geometry. Ip from parameters. current, heat, and particle transport. qlknn transport model. Pedestal. Particle sources including NBI. PC method for density. D_e scaled from chi_e
def test_36(tmpdir):
    runtest(36, tmpdir)

# test37: CHEASE geometry. Ip from parameters. current, heat, and particle transport. qlknn transport model. Pedestal. Particle sources including NBI. PC method for density. Deff + Veff model.
def test_37(tmpdir):
    runtest(37, tmpdir)

# test38: CHEASE geometry. Ip from parameters. current, heat, and particle transport. CGM transport model. Pedestal. Particle sources including NBI. Self-consistent fusion power.
def test_38(tmpdir):
    runtest(38, tmpdir)

# test39: CHEASE geometry. Ip from parameters. current, heat, and particle transport. qlknn transport model. Pedestal. Particle sources including NBI. PC method for density. D_e scaled from chi_e
def test_39(tmpdir):
    runtest(39, tmpdir)

# test40: Mock-up of ITER baseline with Tped=4
def test_40(tmpdir):
    runtest(40, tmpdir)

# test41: ITER baseline based (roughly) on Mantica PPCF 2021
def test_41(tmpdir):
    runtest(41, tmpdir)

# test42: ITER hybrid scenario based (roughly) on van Mulders Nucl. Fusion 2021
def test_42(tmpdir):
    runtest(42, tmpdir)

import numpy as np
from IPython import embed
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.widgets import Slider
import h5py
import argparse

# plotting tool with time slider. Reads output h5 file

# Plots (1) chi_i, chi_e (transport coefficients), (2) Ti, Te (temperatures), (3) ne (density),
# (4) j (toroidal plasma current [total only]) , (5) q (safety factor), (6) s (magnetic shear)

# read h5 file to be ploted from parser (optional)
parser = argparse.ArgumentParser(description='Plot finished run')
parser.add_argument("--outfile", nargs='*', help="Relative location of output h5 files (if two are provided, a comparison is done)")
parser.add_argument("--chimax", help="Maximum chi value in plots")
parser.add_argument("--explicit", action="store_true", help="Plot fipy vs numpy explicit solution")
parser.add_argument('--normalized', action='store_true')
parser.add_argument('--unnormalized', dest='normalized', action='store_false')
parser.set_defaults(normalized=True)

args = parser.parse_args()
if len(args.outfile) > 1: # only the second argument will be processed for the comparison plots
    comp_plot = True
else:
    comp_plot = False

if args.outfile == None:
    raise Exception('No output file provided')

with h5py.File(args.outfile[0]+'.h5', 'r') as hf:
    chii = hf['chii'][:]
    chie = hf['chie'][:]
    Ti = hf['Ti'][:]
    Te = hf['Te'][:]
    ne = hf['ne'][:]
    j = hf['j'][:]
    johm = hf['johm'][:]
    q = hf['q'][:]
    s = hf['s'][:]
    if args.explicit:
        Ti_numpy = hf['Ti_explicit'][:]

    t = hf['t'][:]
    if args.normalized:
        try:
            rcell_coord = hf['rcell_norm'][:]
            rface_coord = hf['rface_norm'][:]
        except:
            raise Exception('Normalized rcell and/or rface does not exist in the h5 file. Possibly old h5 file. Try with --unnormalized.')
    else:
        rcell_coord = hf['rcell'][:]
        rface_coord = hf['rface'][:]
    tmin = min(t)
    tmax = max(t)
    ymax_T = np.amax([Ti,Te])
    ymax_n = np.amax(ne)
    ymax_j = np.amax([np.amax(j),np.amax(johm)])
    ymin_j = np.amin([np.amin(j),np.amin(johm)])
    ymin_j = np.amin(j)
    ymax_q = np.amax(q)
    ymax_s = np.amax(s)
    ymin_s = np.amin(s)
    if args.chimax != None:
        chimax = float(args.chimax[0])
    else:
        chimax = np.amax([chii,chie])*1.1
    dt = min(np.diff(t))

if comp_plot:
    with h5py.File(args.outfile[1]+'.h5', 'r') as hf2:
        chii2 = hf2['chii'][:]
        chie2 = hf2['chie'][:]
        Ti2 = hf2['Ti'][:]
        Te2 = hf2['Te'][:]
        ne2 = hf2['ne'][:]
        j2 = hf2['j'][:]
        johm2 = hf2['johm'][:]
        q2 = hf2['q'][:]
        s2 = hf2['s'][:]

        t2 = hf2['t'][:]
        if args.normalized:
            rcell_coord2 = hf2['rcell_norm'][:]
            rface_coord2 = hf2['rface_norm'][:]
        else:
            rcell_coord2 = hf2['rcell'][:]
            rface_coord2 = hf2['rface'][:]
        tmin = min(np.concatenate([t,t2]))
        tmax = max(np.concatenate([t,t2]))
        ymax_T2 = np.amax([Ti2,Te2])
        ymax_n2 = np.amax(ne2)
        ymax_j2 = np.amax([np.amax(j2),np.amax(johm2)])
        ymin_j2 = np.amin([np.amin(j2),np.amin(johm2)])
        ymax_q2 = np.amax(q2)
        ymax_s2 = np.amax(s2)
        ymin_s2 = np.amin(s2)

        ymax_T = max(ymax_T,ymax_T2)
        ymax_n = max(ymax_n,ymax_n2)
        ymax_j = max(ymax_j,ymax_j2)
        ymin_j = max(ymin_j,ymin_j2)
        ymax_q = max(ymax_q,ymax_q2)
        ymax_s = max(ymax_s,ymax_s2)
        ymin_s = max(ymin_s2,ymin_s2)


        if args.chimax != None:
            chimax = float(args.chimax[0])
        else:
            chimax2 = np.amax([chii2,chie2])*1.1
            chimax = max(chimax, chimax2)

        dt2 = min(np.diff(t2))
        dt = min(dt,dt2)


fig = plt.figure(figsize=(15,10))
ax1 = fig.add_subplot(231)
ax2 = fig.add_subplot(232)
ax3 = fig.add_subplot(233)
ax4 = fig.add_subplot(234)
ax5 = fig.add_subplot(235)
ax6 = fig.add_subplot(236)

lines = []
lines2 = []

if comp_plot:
    ax1.set_title('(1)='+args.outfile[0]+', (2)='+args.outfile[1])
else:
    ax1.set_title('(1)='+args.outfile[0])
line, = ax1.plot(rface_coord, chii[0,:],'r',label=r'$\chi_i~(1)$')
lines.append(line)
line, = ax1.plot(rface_coord, chie[0,:],'b',label=r'$\chi_e~(1)$')
lines.append(line)
line, = ax2.plot(rcell_coord, Ti[0,:],'r',label=r'$T_i~(1)$')
lines.append(line)
if not args.explicit:
    line, = ax2.plot(rcell_coord, Te[0,:],'b',label=r'$T_e~(1)$')
else:
    line, = ax2.plot(rcell_coord, Ti_numpy[0,:],'b',label=r'$T_i~numpy~(1)$')
lines.append(line)
line, = ax3.plot(rcell_coord, ne[0,:],'r',label=r'$n_e~(1)$')
lines.append(line)

line, = ax4.plot(rcell_coord, j[0,:],'r',label=r'$j_{tot}~(1)$')
lines.append(line)
line, = ax4.plot(rcell_coord, johm[0,:],'b',label=r'$j_{ohm}~(1)$')
lines.append(line)

line, = ax5.plot(rface_coord, q[0,:],'r',label=r'$q~(1)$')
lines.append(line)

line, = ax6.plot(rface_coord, s[0,:],'r',label=r'$\hat{s}~(1)$')
lines.append(line)

if comp_plot:
    line, = ax1.plot(rface_coord2, chii2[0,:],'r--',label=r'$\chi_i (2)$')
    lines2.append(line)
    line, = ax1.plot(rface_coord2, chie2[0,:],'b--',label=r'$\chi_e (2)$')
    lines2.append(line)
    line, = ax2.plot(rcell_coord2, Ti2[0,:],'r--',label=r'$T_i (2)$')
    lines2.append(line)
    line, = ax2.plot(rcell_coord2, Te2[0,:],'b--',label=r'$T_e (2)$')
    lines2.append(line)
    line, = ax3.plot(rcell_coord, ne2[0,:],'r--',label=r'$n_e (2)$')
    lines2.append(line)
    line, = ax4.plot(rcell_coord2, j2[0,:],'r--',label=r'$j_{tot} (2)$')
    lines2.append(line)
    line, = ax4.plot(rcell_coord2, johm2[0,:],'b--',label=r'$j_{ohm} (2)$')
    lines2.append(line)
    line, = ax5.plot(rface_coord2, q2[0,:],'r--',label=r'$q (2)$')
    lines2.append(line)
    line, = ax6.plot(rface_coord2, s2[0,:],'r--',label=r'$\hat{s} (2)$')
    lines2.append(line)


ax1.set_ylim([0, chimax])
ax1.set_xlabel("Normalized radius")
ax1.set_ylabel(r"Heat conductivity $[m^2/s]$")
ax1.legend()

ax2.set_ylim([0, ymax_T*1.05])
ax2.set_xlabel("Normalized radius")
ax2.set_ylabel("Temperature [keV]")
ax2.legend()

ax3.set_ylim([0, ymax_n*1.05])
ax3.set_xlabel("Normalized radius")
ax3.set_ylabel(r"Electron density $[10^{20}~m^{-3}]$")
ax3.legend()


ax4.set_ylim([min(ymin_j*1.05,0), ymax_j*1.05])
ax4.set_xlabel("Normalized radius")
ax4.set_ylabel(r"Toroidal current $[A~m^{-2}]$")
ax4.legend()

ax5.set_ylim([0, ymax_q*1.05])
ax5.set_xlabel("Normalized radius")
ax5.set_ylabel("Safety factor")
ax5.legend()

ax6.set_ylim([min(ymin_s*1.05,0), ymax_s*1.05])
ax6.set_xlabel("Normalized radius")
ax6.set_ylabel("Magnetic shear")
ax6.legend()


plt.subplots_adjust(bottom=0.2)
axslide = plt.axes([0.12, 0.05, 0.75, 0.05])

timeslider = Slider(axslide, 'Time [s]', tmin, tmax, valinit=tmin, valstep=dt)

fig.canvas.draw()

def update(val):
    newtime = timeslider.val
    idx = np.abs(t - newtime).argmin() # find index closest to new time
    if not args.explicit:
        datalist = [chii[idx,:],chie[idx,:],Ti[idx,:],Te[idx,:],ne[idx,:],j[idx,:],johm[idx,:],q[idx,:],s[idx,:]]
    else:
        datalist = [chii[idx,:],chie[idx,:],Ti[idx,:],Ti_numpy[idx,:]]
    for line, data in zip(lines, datalist):
        line.set_ydata(data)
    if comp_plot:
        idx = np.abs(t2 - newtime).argmin() # find index closest to new time
        datalist2 = [chii2[idx,:],chie2[idx,:],Ti2[idx,:],Te2[idx,:],ne2[idx,:],j2[idx,:],johm2[idx,:],q2[idx,:],s2[idx,:]]
        for line, data in zip(lines2, datalist2):
            line.set_ydata(data)
    fig.canvas.draw()

# Call update function when slider value is changed
timeslider.on_changed(update)
plt.show()

# fig.tight_layout()

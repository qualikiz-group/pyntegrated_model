import numpy as np
import os
from fipy import CellVariable, Grid1D, FaceVariable
from IPython import embed
import matplotlib.pyplot as plt
import matplotlib as mpl
import json
import pandas as pd
from dataclasses import dataclass

try:
    from qlknn.models.ffnn import QuaLiKizNDNN
except:
    print('Warning: qlknn package not installed')

# initially empty structure where we will store temperature and density profiles
class Profiles:
    pass

# structure where we store geometry information
class Geometry:
    # initialize geometry structure using input parameters
    def __init__(self, inpars, consts):
        if inpars.geometry_type == 'circular':
            # assumes that r/Rmin = rho_norm where rho is the toroidal flux coordinate
            # r_norm coordinate is r/Rmin in circular, and rho_norm in standard geometry (CHEASE/EQDSK)
            # Define mesh (Slab Uniform 1D with Jacobian = 1)
            self.dr_norm = 1/inpars.nr
            self.mesh = Grid1D(nx=inpars.nr, dx=self.dr_norm) # normalized grid
            self.rmax = inpars.Rmin # radius denormalization constant
            # helper variables for mesh cells and faces
            self.rface_norm = FaceVariable(mesh=self.mesh, value=self.mesh.faceCenters()[0])  # fipy FaceVariable for normalized r
            self.rnp_norm = self.mesh.cellCenters()[0] # numpy variable for normalized r
            self.rcell_norm = CellVariable(mesh=self.mesh, value=self.rnp_norm) # fipy CellVariable

            self.dr = self.dr_norm*self.rmax
            self.rface = FaceVariable(mesh=self.mesh, value=self.rface_norm()*self.rmax) # fipy FaceVariable for r
            self.rnp = self.rnp_norm*self.rmax # numpy variable for normalized r
            self.rcell = CellVariable(mesh=self.mesh, value=self.rnp) # fipy CellVariable

            self.kappa = 1 + self.rnp_norm*(inpars.kappa - 1) # assumed elongation profile on cell grid
            self.kappa_face = 1 + self.rface_norm()*(inpars.kappa - 1) # assumed elongation profile on cell grid
            self.delta_face = np.zeros(inpars.nr + 1) # zero triangularity by definition
            self.volume = 2*consts.pi**2*inpars.Rmaj*self.rnp**2*self.kappa
            self.volume_face = 2*consts.pi**2*inpars.Rmaj*self.rface()**2*self.kappa_face
            self.area = consts.pi*self.rnp**2*self.kappa
            self.area_face = consts.pi*self.rface()**2*self.kappa_face
            self.B0 = inpars.B
            # vpr and vpr_face are FiPy variables since they go into transport equations
            self.vpr = CellVariable(mesh=self.mesh, value=4*consts.pi**2*inpars.Rmaj*self.rnp*self.kappa + self.volume/self.kappa*(inpars.kappa - 1)/self.rmax) # V' for volume integrations on cell grid
            self.vpr_face = FaceVariable(mesh=self.mesh, value=4*consts.pi**2*inpars.Rmaj*self.rface()*self.kappa_face + self.volume_face/self.kappa_face*(inpars.kappa - 1)/self.rmax) # V' for volume integrations on face grid

            self.spr = 2*consts.pi*self.rnp*self.kappa + self.area/self.kappa*(inpars.kappa - 1)/self.rmax # S' for area integrals on cell grid
            self.spr_face = 2*consts.pi*self.rface()*self.kappa_face + self.area_face/self.kappa_face*(inpars.kappa - 1)/self.rmax # S' for area integrals on face grid
            self.G2 = self.vpr()/(4*consts.pi**2*inpars.Rmaj**2*np.sqrt(1-(self.rnp/inpars.Rmaj)**2)) # uses <1/R^2> with circular geometry

            # generate G2_face by hand
            self.G2_face = np.zeros(inpars.nr + 1)
            for i in range(1 ,inpars.nr):
                self.G2_face[i] = 0.5*(self.G2[i-1]+self.G2[i])
            self.G2_face[-1] = self.vpr_face()[-1]/(4*consts.pi**2*inpars.Rmaj**2*np.sqrt(1-(self.rface()[-1]/inpars.Rmaj)**2))

            # g0 + g1 variables needed for general geometry form of transport equations
            self.g0 = self.vpr()
            self.g0_face = self.vpr_face()

            self.g1 = self.vpr()**2
            self.g1_face = self.vpr_face()**2

            self.J = np.ones(len(self.rcell()))  # simplifying assumption for now, for J=R*B/(R0*B0)
            self.J_face = np.ones(len(self.rface())) # simplifying assumption for now, for J=R*B/(R0*B0)
            self.F = np.ones(len(self.rcell()))*inpars.Rmaj*self.B0 # simplified (constant) version of the F=B*R function
            self.F_face = np.ones(len(self.rface()))*inpars.Rmaj*self.B0 # simplified (constant) version of the F=B*R function

            # hires versions for j (plasma current) and psi (poloidal flux) manipulations. From [0,1] like face grid
            self.r_hires_norm = np.linspace(0,1, inpars.nr*4)
            self.r_hires = self.r_hires_norm*self.rmax
            self.kappa_hires = 1 + self.r_hires_norm*(inpars.kappa - 1) # assumed elongation profile on hires grid
            self.volume_hires = 2*consts.pi**2*inpars.Rmaj*self.r_hires**2*self.kappa_hires
            self.area_hires = consts.pi*self.r_hires**2*self.kappa_hires

            self.vpr_hires = 4*consts.pi**2*inpars.Rmaj*self.r_hires*self.kappa_hires + self.volume_hires/self.kappa_hires*(inpars.kappa - 1)/self.rmax # V' for volume integrations on hires grid
            self.spr_hires = 2*consts.pi*self.r_hires*self.kappa_hires + self.area_hires/self.kappa_hires*(inpars.kappa - 1)/self.rmax # S' for area integrals on hires grid
            self.G2_hires = self.vpr_hires/(4*consts.pi**2*inpars.Rmaj**2*np.sqrt(1-(self.r_hires/inpars.Rmaj)**2)) # uses <1/R^2> with circular geometry

            # repeated terms in transport equations and dt calculation. Efficient to preevaluate
            self.g1_over_vpr = CellVariable(mesh=self.mesh, value=self.g1/self.vpr())
            self.g1_over_vpr2 = self.g1/self.vpr()**2

            g0_over_vpr_face = np.ones(len(self.g0_face)) # correct value is unity on-axis
            g0_over_vpr_face[1:] = self.g0_face[1:]/self.vpr_face()[1:] # avoid div by zero on-axis
            g1_over_vpr_face_np = np.zeros(len(self.g1_face)) # correct value is zero on-axis
            g1_over_vpr2_face = np.ones(len(self.g1_face)) # correct value is unity on-axis
            g1_over_vpr_face_np[1:] = self.g1_face[1:]/self.vpr_face()[1:] # avoid div by zero on-axis
            g1_over_vpr2_face[1:] = self.g1_face[1:]/self.vpr_face()[1:]**2 # avoid div by zero on-axis
            self.g0_over_vpr_face = g0_over_vpr_face
            self.g1_over_vpr_face = FaceVariable(mesh=self.mesh, value=g1_over_vpr_face_np)
            self.g1_over_vpr2_face = g1_over_vpr2_face

            # needed to use same formulas for both circular and shaped geometry
            self.Rout = inpars.Rmaj + self.rcell()
            self.Rout_face = inpars.Rmaj + self.rface()

            self.Rin = inpars.Rmaj - self.rcell()
            self.Rin_face = inpars.Rmaj - self.rface()

        elif inpars.geometry_type == 'CHEASE':
            # initialize geometry from file
            geodir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))+'/geo/'
            chease_data = initialize_CHEASE_dict(geodir+inpars.geometry_file)

            # prepare variables from CHEASE to be interpolated into our simulation grid
            # CHEASE variables are normalized. Need to unnormalize them with reference values

            self.B0 = inpars.B

            # poloidal flux and CHEASE-internal-calculated plasma current
            psiunnormfactor = (inpars.Rmaj**2*self.B0)*2*consts.pi
            psi_chease = chease_data['PSIchease=psi/2pi']*psiunnormfactor
            Ip_chease = chease_data['Ipprofile']/consts.mu0*inpars.Rmaj*self.B0

            # toroidal flux coordinate
            rho = chease_data['RHO_TOR=sqrt(Phi/pi/B0)']*inpars.Rmaj
            rhon = chease_data['RHO_TOR_NORM']

            # midplane radii
            Rin = chease_data['R_INBOARD']*inpars.Rmaj
            Rout = chease_data['R_OUTBOARD']*inpars.Rmaj

            # toroidal field flux function
            J = chease_data['T=RBphi']
            F = J*inpars.Rmaj*self.B0

            # Triangularity
            delta_upper_face = chease_data['delta_upper']
            delta_lower_face = chease_data['delta_bottom']

            # flux surface integrals of various geometry quantities
            C1 = chease_data['Int(Rdlp/|grad(psi)|)=Int(Jdchi)'] * inpars.Rmaj/self.B0
            C2 = chease_data['<1/R**2>'] * C1 / inpars.Rmaj**2
            C3 = chease_data['<Bp**2>'] * C1 * self.B0**2
            C4 = chease_data['<|grad(psi)|**2>'] * C1 * (self.B0*inpars.Rmaj)**2

            # derived quantities for transport equations and transformations
            g0 = 2*consts.pi * chease_data['<|grad(psi)|>']*self.B0*inpars.Rmaj*C1 # <\nabla V>
            g1 = 4*consts.pi**2*C1*C4 # <(\nabla V)**2>
            g2 = 4*consts.pi**2*C1*C3 # <(\nabla V)**2 / R**2>
            g3 = np.zeros(len(g1)) # <1/R**2>
            g3[1:] = C2[1:]/C1[1:]
            g3[0] = 1/Rin[0]**2 # correct value on-axis due to undefined 0/0 with C2[0]/C1[0]

            G2 = np.zeros(len(g2))
            G2[1:] = inpars.Rmaj/(16*consts.pi**4)*J[1:]*g2[1:]*g3[1:]/rho[1:]
            G2[0] = 0

            # make an alternative initial psi, self-consistent with CHEASE Ip profile
            # needed because CHEASE psi profile has noisy second derivatives
            dpsidrho = np.zeros(len(psi_chease))
            dpsidrho[1:] = Ip_chease[1:]*consts.mu0/G2[1:]
            psi_from_chease_Ip = np.zeros(len(psi_chease))
            for i in range(1,len(psi_from_chease_Ip)+1):
                psi_from_chease_Ip[i-1] = np.trapz(dpsidrho[:i], rho[:i])
            # set Ip-consistent psi derivative boundary condition (although will be replaced later with FiPy constraint)
            psi_from_chease_Ip[-1] = psi_from_chease_Ip[-2] + consts.mu0*Ip_chease[-1]/G2[-1]*(rho[-1]-rho[-2])

            # if Ip from parameter file, renormalize psi to match desired current
            if inpars.Ip_from_parameters:
                Ip_scale_factor = inpars.Ip*1e6/Ip_chease[-1]
                psi_from_chease_Ip *= Ip_scale_factor
            else:
                inpars.Ip = Ip_chease[-1]/1e6
                Ip_scale_factor = 1

            # volume, area, and dV/drho, dS/drho
            volume = chease_data['VOLUMEprofile']*inpars.Rmaj**3
            area = chease_data['areaprofile']*inpars.Rmaj**2
            vpr = np.gradient(volume, rho)
            spr = np.gradient(area, rho)
            vpr[0] = 0 # np.gradient boundary approximation not appropriate here
            spr[0] = 0 # np.gradient boundary approximation not appropriate here

            # plasma current density
            jtot = 2*consts.pi*inpars.Rmaj*np.gradient(Ip_chease, volume)*Ip_scale_factor

               # fill geometry structure
               # r_norm coordinate is rho_tor_norm
            self.dr_norm = 1/inpars.nr
            self.mesh = Grid1D(nx=inpars.nr, dx=self.dr_norm) # normalized grid
            self.rmax = rho[-1] # radius denormalization constant
            # helper variables for mesh cells and faces
            self.rface_norm = FaceVariable(mesh=self.mesh, value=self.mesh.faceCenters()[0])  # fipy FaceVariable for normalized r
            self.rnp_norm = self.mesh.cellCenters()[0] # numpy variable for normalized r
            self.rcell_norm = CellVariable(mesh=self.mesh, value=self.rnp_norm) # fipy CellVariable

            self.dr = self.dr_norm*self.rmax
            self.rface = FaceVariable(mesh=self.mesh, value=self.rface_norm()*self.rmax) # fipy FaceVariable for r
            self.rnp = self.rnp_norm*self.rmax # numpy variable for normalized r
            self.rcell = CellVariable(mesh=self.mesh, value=self.rnp) # fipy CellVariable

            self.vpr = CellVariable(mesh=self.mesh, value=np.interp(self.rcell_norm(), rhon, vpr)) # V' for volume integrations on cell grid
            self.vpr_face = FaceVariable(mesh=self.mesh, value=np.interp(self.rface_norm(), rhon, vpr)) # V' for volume integrations on face grid

            self.spr = CellVariable(mesh=self.mesh, value=np.interp(self.rcell_norm(), rhon, spr)) # S' for area integrations on cell grid
            self.spr_face = FaceVariable(mesh=self.mesh, value=np.interp(self.rface_norm(), rhon, spr)) # S' for area integrations on face grid

            self.G2 = CellVariable(mesh=self.mesh, value=np.interp(self.rcell_norm(), rhon, G2)) 
            self.G2_face = FaceVariable(mesh=self.mesh, value=np.interp(self.rface_norm(), rhon, G2)) 

            self.J = CellVariable(mesh=self.mesh, value=np.interp(self.rcell_norm(), rhon, J)) 
            self.J_face = FaceVariable(mesh=self.mesh, value=np.interp(self.rface_norm(), rhon, J)) 
            self.F = self.J*inpars.Rmaj*self.B0 # simplified (constant) version of the F=B*R function
            self.F_face = self.J_face*inpars.Rmaj*self.B0 # simplified (constant) version of the F=B*R function

            self.psi_chease = np.interp(self.rcell_norm(), rhon, psi_chease)
            self.psi_from_chease_Ip = np.interp(self.rcell_norm(), rhon, psi_from_chease_Ip)

            self.jtot = np.interp(self.rcell_norm(), rhon, jtot)
            self.jtot_face = np.interp(self.rface_norm(), rhon, jtot)

            self.Rin = np.interp(self.rcell_norm(), rhon, Rin)
            self.Rin_face = np.interp(self.rface_norm(), rhon, Rin)

            self.Rout = np.interp(self.rcell_norm(), rhon, Rout)
            self.Rout_face = np.interp(self.rface_norm(), rhon, Rout)

            self.g0 = np.interp(self.rcell_norm(), rhon, g0)
            self.g0_face = np.interp(self.rface_norm(), rhon, g0)

            self.g1 = np.interp(self.rcell_norm(), rhon, g1)
            self.g1_face = np.interp(self.rface_norm(), rhon, g1)

            self.g2 = np.interp(self.rcell_norm(), rhon, g2)

            self.g3 = np.interp(self.rcell_norm(), rhon, g3)

            self.volume = np.interp(self.rcell_norm(), rhon, volume)
            self.volume_face = np.interp(self.rface_norm(), rhon, volume)

            self.area = np.interp(self.rcell_norm(), rhon, area)
            self.area_face = np.interp(self.rface_norm(), rhon, area)

            self.delta_upper_face = np.interp(self.rface_norm(), rhon, delta_upper_face)
            self.delta_lower_face = np.interp(self.rface_norm(), rhon, delta_lower_face)
            self.delta_face = 0.5*(self.delta_upper_face + self.delta_lower_face)

            # repeated terms in transport equations. Efficient to preevaluate
            self.g1_over_vpr = CellVariable(mesh=self.mesh, value=self.g1/self.vpr())
            self.g1_over_vpr2 = self.g1/self.vpr()**2

            g0_over_vpr_face = np.ones(len(self.g0_face)) # correct value is unity on-axis
            g0_over_vpr_face[1:] = self.g0_face[1:]/self.vpr_face()[1:] # avoid div by zero on-axis
            g1_over_vpr_face_np = np.zeros(len(self.g1_face)) # correct value is zero on-axis
            g1_over_vpr2_face = np.ones(len(self.g1_face)) # correct value is unity on-axis
            g1_over_vpr_face_np[1:] = self.g1_face[1:]/self.vpr_face()[1:] # avoid div by zero on-axis
            g1_over_vpr2_face[1:] = self.g1_face[1:]/self.vpr_face()[1:]**2 # avoid div by zero on-axis
            self.g0_over_vpr_face = g0_over_vpr_face
            self.g1_over_vpr_face = FaceVariable(mesh=self.mesh, value=g1_over_vpr_face_np)
            self.g1_over_vpr2_face = g1_over_vpr2_face

        else:
            raise Exception('Non-supported geometry type used')



@dataclass
class Constants:
    # structure for storing physical constants
    mu5: float = 0.0
    pi: float = np.pi
    keV2J: float = 1e3*1.6e-19
    mp: float = 1.67e-27
    qe: float = 1.6e-19
    me: float = 9.11e-31
    epsilon0: float = 8.854e-12
    mu0: float = 4*pi*1e-7
    eps: float = 1e-7 # small number for avoiding division by zeroes

def initialize_CHEASE_dict(filepath):
    with open(filepath, 'r') as file:
        chease_data = {}
        var_labels = file.readline().strip().split()[1:] # ignore % comment column

        for var_label in var_labels:
            chease_data[var_label] = []

        # store data in respective keys
        for line in file:
            values = line.strip().split()
            for var_label,value in zip(var_labels, values):
                chease_data[var_label].append(float(value))

    # Convert lists to numpy arrays
    for var_label in var_labels:
        chease_data[var_label] = np.array(chease_data[var_label])

    return chease_data

def test_CHEASE_equilibrium(inpars, geo, consts, profiles):
    '''
    Calculates various magnetic equilibrium quantities from
    both CHEASE data directly, and from PINT geo and profiles structs.
    Plots comparisons for sanity checking
    '''

    # reinitialize geometry from file
    geodir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))+'/geo/'
    chease_data = initialize_CHEASE_dict(geodir+inpars.geometry_file)

    psiunnormfactor = (inpars.Rmaj**2*geo.B0)*2*consts.pi
    psi_chease = chease_data['PSIchease=psi/2pi']*psiunnormfactor

    rho = chease_data['RHO_TOR=sqrt(Phi/pi/B0)']*inpars.Rmaj
    rhon = chease_data['RHO_TOR_NORM']
    J = chease_data['T=RBphi']
    F = J*inpars.Rmaj*geo.B0

    Rin = chease_data['R_INBOARD']*inpars.Rmaj
    Rout = chease_data['R_OUTBOARD']*inpars.Rmaj

    C1 = chease_data['Int(Rdlp/|grad(psi)|)=Int(Jdchi)'] * inpars.Rmaj/geo.B0
    C2 = chease_data['<1/R**2>'] * C1 / inpars.Rmaj**2
    C3 = chease_data['<Bp**2>'] * C1 * geo.B0**2
    C4 = chease_data['<|grad(psi)|**2>'] * C1 * (geo.B0*inpars.Rmaj)**2

    g1 = 4*consts.pi**2*C1*C4
    g2 = 4*consts.pi**2*C1*C3

    g3 = np.zeros(len(g1))
    g3[1:] = C2[1:]/C1[1:]
    g3[0] = 1/Rin[0]**2 # correct value on-axis due to undefined 0/0 with C2[0]/C1[0]
    G2 = np.zeros(len(g2))
    G2[1:] = inpars.Rmaj/(16*consts.pi**4)*J[1:]*g2[1:]*g3[1:]/rho[1:]
    G2[0] = 0

    volume = chease_data['VOLUMEprofile']*inpars.Rmaj**3
    area = chease_data['areaprofile']*inpars.Rmaj**2

    vpr = np.gradient(volume, rho)
    spr = np.gradient(area, rho)
    vpr[0] = 0 # np.gradient boundary approximation not appropriate here
    spr[0] = 0 # np.gradient boundary approximation not appropriate here

    q_chease = chease_data['Qprofile']
    s_chease = chease_data['SHEARprofile']

    # Ip calculated directly from chease psi
    Ip_from_chease_psi = G2/consts.mu0*np.gradient(psi_chease,rho)

    # Ip calculated from PINT psi initialized earlier
    Ip_from_pint_psi = profiles.psi.faceGrad()[0]/geo.rmax*geo.G2_face/consts.mu0

    # Ip directly from CHEASE data
    Ip_chease = chease_data['Ipprofile']/consts.mu0*inpars.Rmaj*geo.B0

    # current density from CHEASE Ip
    jtot_from_chease_Ip = 2*consts.pi*inpars.Rmaj*np.gradient(Ip_chease, volume)

    # current density from CHEASE psi (second derivatives)
    jtot_from_chease_psi = 2*consts.pi*inpars.Rmaj*np.gradient(Ip_from_chease_psi, volume)

    # make an alternative initial psi, self-consistent with CHEASE Ip profile
    # needed because CHEASE psi profile has noisy second derivatives
    dpsidrho = np.zeros(len(psi_chease))
    dpsidrho[1:] = Ip_chease[1:]*consts.mu0/G2[1:]
    psi_from_chease_Ip = np.zeros(len(psi_chease))
    for i in range(1,len(psi_from_chease_Ip)+1):
        psi_from_chease_Ip[i-1] = np.trapz(dpsidrho[:i], rho[:i])
    # set consistent psi derivative boundary condition
    psi_from_chease_Ip[-1] = psi_from_chease_Ip[-2] + consts.mu0*Ip_chease[-1]/G2[-1]*(rho[-1]-rho[-2])

    # calculate the back transformation to check consistency
    Ip_from_psi_from_chease_Ip = G2/consts.mu0*np.gradient(psi_from_chease_Ip, rho)

    # calculate the current density from the new derived CHEASE psi
    jtot_from_psi_from_chease_Ip = 2*consts.pi*inpars.Rmaj*np.gradient(Ip_from_psi_from_chease_Ip, volume)

    # if Ip from parameter file, renormalize psi to match desired current
    if inpars.Ip_from_parameters:
        Ip_scale_factor = inpars.Ip*1e6/Ip_chease[-1]
        psi_from_chease_Ip_rescaled = psi_from_chease_Ip*Ip_scale_factor
        Ip_from_psi_from_chease_Ip_rescaled = G2/consts.mu0*np.gradient(psi_from_chease_Ip_rescaled, rho)
        jtot_from_psi_from_chease_Ip_rescaled = 2*consts.pi*inpars.Rmaj*np.gradient(Ip_from_psi_from_chease_Ip_rescaled, volume)
    else:
        inpars.Ip = Ip_chease[-1]/1e6

    # calculate q based purely on original CHEASE psi profile
    iota = np.zeros(len(rho))
    iota[1:] = np.gradient(psi_chease,rho)[1:]/(2*consts.pi*geo.B0*rho[1:])
    q_from_chease_psi = np.zeros(len(iota))
    q_from_chease_psi[1:] = 1/iota[1:]
    q_from_chease_psi[0] = 2*geo.B0/(consts.mu0*jtot_from_chease_psi[0]*inpars.Rmaj)

    # calculate q based on psi calculated from CHEASE Ip profile
    iota = np.zeros(len(rho))
    iota[1:] = np.gradient(psi_from_chease_Ip,rho)[1:]/(2*consts.pi*geo.B0*rho[1:])
    q_from_psi_from_chease_Ip = np.zeros(len(iota))
    q_from_psi_from_chease_Ip[1:] = 1/iota[1:]
    q_from_psi_from_chease_Ip[0] = 2*geo.B0/(consts.mu0*jtot_from_psi_from_chease_Ip[0]*inpars.Rmaj)

    if inpars.Ip_from_parameters:
        # calculate q based on rescaled CHEASE psi(from Ip) profile
        iota = np.zeros(len(rho))
        iota[1:] = np.gradient(psi_from_chease_Ip_rescaled,rho)[1:]/(2*consts.pi*geo.B0*rho[1:])
        q_from_psi_from_chease_Ip_rescaled = np.zeros(len(iota))
        q_from_psi_from_chease_Ip_rescaled[1:] = 1/iota[1:]
        q_from_psi_from_chease_Ip_rescaled[0] = 2*geo.B0/(consts.mu0*jtot_from_psi_from_chease_Ip_rescaled[0]*inpars.Rmaj)

    # calculate q based on other CHEASE geometry profiles (not psi directly)
    q_from_C_chease = 1/(2*consts.pi)*F*C2
    q_from_C_chease[0] = 2*geo.B0/(consts.mu0*jtot_from_chease_psi[0]*inpars.Rmaj)

    # alternative formula for Ip based on CHEASE geometry profiles
    Ip_fromFgq = F*g2*g3/q_from_C_chease/consts.mu0/(2*consts.pi)**3

    # parallel current and current density from CHEASE data directly
    jpar_chease = chease_data['j//=<J.B>/B0']/(consts.mu0*inpars.Rmaj)*geo.B0
    Ipar = np.zeros(len(jpar_chease))
    for i in range(len(jpar_chease)):
        Ipar[i] = np.trapz(spr[:i+1]*jpar_chease[:i+1], rho[:i+1])

    # calculate pint j, q, s profiles for comparison to CHEASE
    jtot_face_from_pint, jtot_from_pint_psi = calc_jtot_from_psi(inpars, geo, consts, profiles)
    q_face_from_pint, _ = calc_q_from_psi(inpars, geo, consts, profiles)
    s_face_from_pint = calc_s_from_psi(inpars, geo, consts, profiles)

    #embed()

    fs = 10
    fig = plt.figure(figsize=(15,10))
    ax1 = fig.add_subplot(231)
    ax2 = fig.add_subplot(232)
    ax3 = fig.add_subplot(233)
    ax4 = fig.add_subplot(234)
    ax5 = fig.add_subplot(235)
    ax6 = fig.add_subplot(236)

    ax1.plot(rhon, psi_chease, 'r',label='$\psi$ directly from CHEASE data')
    ax1.plot(geo.rface_norm(), profiles.psi.faceValue(), 'b',label='$\psi$ in PINT')
    ax1.plot(rhon, psi_from_chease_Ip, 'g',label='$\psi$ derived from CHEASE $I_p$')
    if inpars.Ip_from_parameters:
        ax1.plot(rhon, psi_from_chease_Ip_rescaled, 'y',label='$\psi$ derived from CHEASE $I_p$ rescaled')
    ax1.set_xlabel(r'$\rho_N$', fontsize=fs)
    ax1.set_ylabel(r'$\psi$ [Wb]', fontsize=fs)
    ax1.legend(fontsize=fs)

    ax2.plot(rhon, q_chease, 'r', label='$q$ in CHEASE data')
    ax2.plot(geo.rface_norm(), q_face_from_pint, 'b', label='$q$ from PINT $\psi$')
    ax2.plot(rhon, q_from_chease_psi, 'g', label='$q$ from CHEASE $\psi$')
    ax2.plot(rhon, q_from_psi_from_chease_Ip, 'y', label='$q$ from $\psi$ from CHEASE $I_p$')
    if inpars.Ip_from_parameters:
        ax2.plot(rhon, q_from_psi_from_chease_Ip_rescaled, 'm', label='$q$ from $\psi$ from CHEASE $I_p$ rescaled')
    ax2.plot(rhon, q_from_C_chease, 'k', label='$q$ from $C_2$ and $F$')
    ax2.set_xlabel(r'$\rho_N$', fontsize=fs)
    ax2.set_ylabel(r'$q$', fontsize=fs)
    ax2.legend(fontsize=fs)

    ax3.plot(rhon, F, 'r', label='F from CHEASE data')
    ax3.set_xlabel(r'$\rho_N$', fontsize=fs)
    ax3.set_ylabel(r'$F$', fontsize=fs)
    ax3.legend(fontsize=fs)

    ax4.plot(rhon, Ip_chease/1e6, 'r', label=r'$I_p$ CHEASE')
    ax4.plot(rhon, Ip_from_chease_psi/1e6, 'b', label=r'$I_{p,tor}=\frac{G_2}{\mu_0}\frac{\partial \psi}{\partial \rho}$ from CHEASE')
    ax4.plot(geo.rface_norm(), Ip_from_pint_psi/1e6, 'g', label=r'$I_{p,tor}=\frac{G_2}{\mu_0}\frac{\partial \psi}{\partial \rho}$ from PINT')
    ax4.plot(rhon, Ip_from_psi_from_chease_Ip/1e6, 'y', label=r'$I_p$ from $\psi$ derived from CHEASE $I_p$')
    ax4.plot(rhon, Ip_fromFgq/1e6, 'b', label=r'$I_{p,tor}$ from CHEASE F,g2,g3,q')
    ax4.set_xlabel(r'$\rho_N$', fontsize=fs)
    ax4.set_ylabel(r'$I_p~[MA]$', fontsize=fs)
    ax4.legend(fontsize=fs-2)

    # current
    ax5.plot(rhon, jtot_from_chease_Ip, 'r',label='$j_{tot}$ from CHEASE $I_p$')
    ax5.plot(geo.rface_norm(), jtot_face_from_pint, 'b',label='$j_{tot}$ from PINT $\psi$')
    ax5.plot(rhon, jtot_from_chease_psi, 'g',label='$j_{tot}$ from CHEASE $\psi$')
    ax5.plot(rhon, jtot_from_psi_from_chease_Ip, 'y',label='$j_{tot}$ from $\psi$ from CHEASE $I_p$')
    if inpars.Ip_from_parameters:
        ax5.plot(rhon, jtot_from_psi_from_chease_Ip_rescaled, 'm',label='$j_{tot}$ from $\psi$ from CHEASE $I_p$ rescaled')
    ax5.set_xlabel(r'$\rho_N$', fontsize=fs)
    ax5.set_ylabel(r'$j~[A/m^2]$', fontsize=fs)
    ax5.legend(fontsize=fs)

    # magnetic shear
    ax6.plot(rhon, s_chease, 'r',label='s in CHEASE data')
    ax6.plot(geo.rface_norm(), s_face_from_pint, 'b',label='s from PINT $\psi$')
    ax6.set_xlabel(r'$\rho_N$', fontsize=fs)
    ax6.set_ylabel(r'Magnetic shear $\hat{s}$', fontsize=fs)
    ax6.legend(fontsize=fs)

    fig.tight_layout()

    plt.show()
    #embed()

def face_to_cell(inpars, face):
    # helper function for converting numpy array from face to cell grid
    cell = np.zeros(inpars.nr)
    cell[:] = 0.5*(face[1:]+face[:-1])
    return cell

# routine for initializing temperature profiles
def init_profiles(inpars, geo, consts):
    profiles = Profiles() # instantiate profiles structure
    initial_T_face = np.linspace(inpars.T_bound_left, inpars.T_bound_right, inpars.nr + 1) # set initial condition on the face grid
    initial_T = 0.5*(initial_T_face[:-1] + initial_T_face[1:]) # set initial condition on the cell grid
    profiles.Ti = CellVariable(name="Ion Temperature", mesh=geo.mesh, value=initial_T) # define Ti profile as fipy CellVariable on the mesh stored in geo.mesh
    profiles.Te = CellVariable(name="Electron Temperature", mesh=geo.mesh, value=initial_T) # define Te profile as fipy CellVariable on the mesh stored in geo.mesh

    # Boundary conditions: zero gradient boundary condition for r=0 is the default, thus not needed to set it explicitly
    profiles.Ti.constrain(inpars.T_bound_right, geo.mesh.facesRight) # set Dirichlet boundary condtion for r=Rmin for Ti
    profiles.Te.constrain(inpars.T_bound_right, geo.mesh.facesRight) # set Dirichlet boundary condtion for r=Rmin for Te

    # set up density profile
    if inpars.set_fGW: # set nbar to desired fraction of Greenwald density of set_fGW=True
        nGW = inpars.Ip/ ( np.pi * inpars.Rmin**2) * 1e20
        nbar = inpars.fGW*nGW / inpars.nref # normalized nbar
    else:
        nbar = inpars.nbar

    nshape_face = np.linspace(inpars.npeak, 1, inpars.nr + 1) # set peaking on face grid (limited to linear profile)
    nshape = 0.5*(nshape_face[:-1] + nshape_face[1:]) # set peaking on cell grid

    # Find normalization factor such that desired line-averaged n is set
    # Assumes line-averaged central chord on outer midplane
    geo.Rmin_out = geo.Rout_face[-1]-geo.Rout_face[0]
    C = nbar / (np.trapz(nshape_face, geo.Rout_face) / geo.Rmin_out)

    profiles.ne = CellVariable(name="Electron Density", mesh=geo.mesh, value=C*nshape) # electron density
    profiles.ne.constrain(inpars.ne_bound_right, geo.mesh.facesRight) # set Dirichlet boundary condition for r=Rmin for ne

    # define ion profile based on (flat) Zeff and single assumed impurity with Zimp
    # main ion limited to hydrogenic species for now. Assume isotopic balance for DT fusion power
    # solve for ni based on: Zeff = (ni + Zimp**2 * nimp)/ne  ;  nimp*Zimp + ni = ne
    dilution_factor = (inpars.Zimp - inpars.Zeff)/(inpars.Zimp - 1)
    profiles.ni = CellVariable(name="Main Ion Density", mesh=geo.mesh, value=profiles.ne()*dilution_factor) # electron density
    profiles.ni.constrain(inpars.ne_bound_right*dilution_factor, geo.mesh.facesRight) # set Dirichlet boundary condition for r=Rmin for ne

    if inpars.geometry_type == 'circular':

        # set up initial current profile without bootstrap current, to get q-profile approximation (needed for bootstrap)
        geo = init_currents(inpars, geo, consts, profiles, bootstrap=False, hires=True)

        # set up initial psi profile (from current profile without bootstrap current)
        initial_psi = calc_psi_from_jtot(inpars, geo, consts)
        profiles.psi = CellVariable(name="Poloidal Flux", mesh=geo.mesh, value=initial_psi)
        profiles.psi.faceGrad.constrain(inpars.Ip*1e6*consts.mu0/geo.G2_face[-1]*geo.rmax, where=geo.mesh.facesRight)
        geo.q, geo.q_cell = calc_q_from_psi(inpars, geo, consts, profiles)

        # second iteration, with bootstrap current
        geo = init_currents(inpars, geo, consts, profiles, bootstrap=True, hires=True)

        # set up initial psi profile (second iteration from current profile with bootstrap current)
        initial_psi = calc_psi_from_jtot(inpars, geo, consts)
        profiles.psi = CellVariable(name="Poloidal Flux", mesh=geo.mesh, value=initial_psi)
        profiles.psi.faceGrad.constrain(inpars.Ip*1e6*consts.mu0/geo.G2_face[-1]*geo.rmax, where=geo.mesh.facesRight)
        geo.q, geo.q_cell = calc_q_from_psi(inpars, geo, consts, profiles)

    if inpars.geometry_type == 'CHEASE':
        # psi is already provided from the CHEASE equilibrium, so no need to first calculate currents
        # however, non-inductive currents are still calculated and used in current diffusion equation
        profiles.psi = CellVariable(name="Poloidal Flux", mesh=geo.mesh, value=geo.psi_from_chease_Ip)
        profiles.psi.faceGrad.constrain(inpars.Ip*1e6*consts.mu0/geo.G2_face[-1]*geo.rmax, where=geo.mesh.facesRight)
        geo.q, geo.q_cell = calc_q_from_psi(inpars, geo, consts, profiles)

        # calculation external currents
        geo = init_currents(inpars, geo, consts, profiles, bootstrap=True, hires=False)

    return profiles, geo


def init_currents(inpars, geo, consts, profiles, bootstrap, hires):

    nr_hires = inpars.nr * inpars.hires_fac # hires grid for more accurate manipulations of current and poloidal flux

    if bootstrap:
        _, j_bootstrap, j_bootstrap_face = calc_neoclassical(inpars, geo, consts, profiles)
        if hires:
            j_bootstrap_hires = np.interp(geo.r_hires, geo.rface(), j_bootstrap_face) # on hires grid
        I_bootstrap = np.trapz(j_bootstrap_face*geo.spr_face, geo.rface())
        f_bootstrap = I_bootstrap/(inpars.Ip*1e6)
    else:
        j_bootstrap = 0
        j_bootstrap_face = 0
        if hires:
            j_bootstrap_hires = 0
        I_bootstrap = 0
        f_bootstrap = 0

    Iohm = inpars.Ip*(1 - inpars.fext - f_bootstrap) # total Ohmic current MA
    Iext = inpars.Ip*inpars.fext # total external current MA

    # calculate "Ohmic" current profile
    johmform_face = (1 - geo.rface_norm()**2)**inpars.nu # form of Ohmic current on cell grid
    Cohm = Iohm*1e6 / np.trapz(johmform_face*geo.spr_face, geo.rface())
    johm_face = Cohm*johmform_face # ohmic current profile on face grid
    johm = face_to_cell(inpars, johm_face) # on cell grid

    # calculate "External" current profile (e.g. ECCD)
    if np.abs(inpars.fext) > 0:
        jextform_face = np.exp(- (geo.rface_norm() - inpars.rext)**2/(2*inpars.wext**2)) # form of external current on cell grid
        Cext = Iext*1e6 / np.trapz(jextform_face*geo.spr_face, geo.rface())
        jext_face = Cext*jextform_face # external current profile on face grid
        jext = face_to_cell(inpars, jext_face) # on cell grid
    else:
        jext_face = np.zeros(len(johm_face))
        jext = np.zeros(len(johm))


    jtot_face = johm_face + jext_face + j_bootstrap_face # on face grid
    jtot = face_to_cell(inpars, jtot_face) # on cell grid

    if hires: # do hi res version for circular geometry psi initialization (not needed for CHEASE)

        # calculate hires "Ohmic" current profile
        johmform_hires = (1 - geo.r_hires_norm**2)**inpars.nu # form of Ohmic current on cell grid
        Cohm = Iohm*1e6 / np.trapz(johmform_hires*geo.spr_hires, geo.r_hires)
        johm_hires = Cohm*johmform_hires # ohmic current profile on hires grid

        # calculate "External" current profile (e.g. ECCD)
        jextform_hires = np.exp(- (geo.r_hires_norm - inpars.rext)**2/(2*inpars.wext**2)) # form of external current on cell grid

        Cext = Iext*1e6 / np.trapz(jextform_hires*geo.spr_hires, geo.r_hires)
        jext_hires = Cext*jextform_hires # external current profile on hires grid

        geo.jtot_hires = johm_hires + jext_hires + j_bootstrap_hires # high resolution jtor for more accurate initial poloidal flux

    geo.j_bootstrap = j_bootstrap
    geo.j_bootstrap_face = j_bootstrap_face
    geo.jext = jext
    geo.jext_face = jext_face
    geo.johm = johm
    geo.johm_face = johm_face
    geo.jtot = jtot
    geo.jtot_face = jtot_face

    return geo

def calc_psi_from_jtot(inpars, geo, consts):
    ''' Set initial psi condition. Used only with circular geometry.
    indx is j index in geo for psi calculation (0 for initial condition).
    For increased accuracy of psi, hires grid is used (due to double integration)
    '''
    dpsi_dr_hires = np.zeros(inpars.nr*inpars.hires_fac)
    psi = np.zeros(inpars.nr) # psi0 = 0 for initial time
    psi_hires = np.zeros(inpars.nr*inpars.hires_fac) # psi0 = 0 for initial time
    for i in range(1,inpars.nr*inpars.hires_fac):
        # dpsi_dr on the cell grid
        dpsi_dr_hires[i] = consts.mu0/(2*np.pi*inpars.Rmaj*geo.G2_hires[i])*np.trapz(geo.jtot_hires[:i+1]*geo.vpr_hires[:i+1], geo.r_hires[:i+1])

    # psi on cell grid
    for i in range(inpars.nr*inpars.hires_fac):
        psi_hires[i] = np.trapz(dpsi_dr_hires[:i+1], geo.r_hires[:i+1])
  
    psi = np.interp(geo.rnp, geo.r_hires, psi_hires)  # on cell grid

    return psi

def calc_jtot_from_psi(inpars, geo, consts, profiles):
    # on face grid
    dpsi_dr = profiles.psi.faceGrad()[0]/geo.rmax

    # inside flux surface on face grid
    I_tot = dpsi_dr*geo.G2_face/consts.mu0

    jtot_face = 2*consts.pi*inpars.Rmaj*np.gradient(I_tot, geo.volume_face)
    jtot_cell = face_to_cell(inpars, jtot_face)

    return jtot_face, jtot_cell

def calc_q_from_psi(inpars, geo, consts, profiles):
    iota = np.zeros(inpars.nr+1) # on face grid
    q_face = np.zeros(inpars.nr+1) # on face grid
    dpsi_dr = profiles.psi.faceGrad()[0]/geo.rmax
    iota[1:] = np.abs(dpsi_dr[1:]/(2*np.pi*geo.B0*geo.rface()[1:]))
    q_face[1:] = 1/iota[1:]
    q_face[0] = 2*geo.B0/(consts.mu0*geo.jtot_face[0]*inpars.Rmaj)  # use on-axis definition of q (Wesson 2004, Eq 3.48)
    if inpars.geometry_type == 'circular':
        q_face *= inpars.q_correction_factor
    q_cell = face_to_cell(inpars, q_face)
    return q_face, q_cell

def calc_s_from_psi(inpars, geo, consts, profiles):

    s = np.zeros(inpars.nr+1) # on face grid
    V = np.zeros(inpars.nr+1) # volume on face grid

    dpsi_dr = profiles.psi.faceGrad()[0]/geo.rmax # 1st derivative of psi on face grid
    d2psi_d2r = np.zeros(inpars.nr+1) # 2nd derivative of psi on face grid

    for i in range(1,inpars.nr): # calculate 2nd derivatives
        d2psi_d2r[i] = (dpsi_dr[i+1] - dpsi_dr[i-1])/(2*geo.dr)
    d2psi_d2r[0] = d2psi_d2r[1]
    d2psi_d2r[-1] = d2psi_d2r[-2]

    for i in range(inpars.nr+1):
        V[i] = np.trapz(geo.vpr_face()[:i+1], geo.rface()[:i+1])

    s[1:] = 2*V[1:]/(geo.rface()[1:]*geo.vpr_face()[1:])*(1 - geo.rface()[1:]*d2psi_d2r[1:]/dpsi_dr[1:])
    s[0] = s[1]

    return s # similar values to s from the psi derivatives but offset by a cell and differences near edge regions. Use direct from q for now


def calculate_source(inpars, geo):

    Q_cell = np.exp(-(geo.rnp-inpars.rsource)**2/(2*inpars.w**2)) # calculate heat profile
    Q_face = np.exp(-(geo.rface()-inpars.rsource)**2/(2*inpars.w**2)) # calculate heat profile (facegrid)
    C = inpars.Ptot / np.trapz(geo.vpr_face()*Q_face, geo.rface()) # calculate constant prefactor

    source_ion = CellVariable(mesh=geo.mesh, value=C*Q_cell*inpars.ion_heat_ratio)
    source_el = CellVariable(mesh=geo.mesh, value=C*Q_cell*inpars.el_heat_ratio)

    return source_ion, source_el

def calculate_particle_sources(inpars, geo):

    S_pellet = np.exp(-(geo.rnp_norm-inpars.pellet_deposition_location)**2/(2*inpars.pellet_width**2)) # calculate gas puff deposition profile
    S_pellet_face = np.exp(-(geo.rface_norm()-inpars.pellet_deposition_location)**2/(2*inpars.pellet_width**2)) # calculate gas puff deposition profile (facegrid)
    C = inpars.S_pellet_tot / np.trapz(geo.vpr_face()*S_pellet_face, geo.rface()) # calculate constant prefactor

    pellet_source = CellVariable(mesh=geo.mesh, value=C*S_pellet) / inpars.nref # normalize to nref with rest of density equation

    S_puff = np.exp(-(1 - geo.rnp_norm)/inpars.puff_decay_length) # calculate pellet deposition profile

    S_puff_face = np.exp(-(1 - geo.rface_norm())/inpars.puff_decay_length) # calculate pellet deposition profile (facegrid)
    C = inpars.S_puff_tot / np.trapz(geo.vpr_face()*S_puff_face, geo.rface()) # calculate constant prefactor

    puff_source = CellVariable(mesh=geo.mesh, value=C*S_puff) / inpars.nref # normalize to nref with rest of density equation

    S_nbi = np.exp(-(geo.rnp_norm-inpars.nbi_deposition_location)**2/(2*inpars.nbi_particle_width**2)) # calculate nbi particle deposition profile
    S_nbi_face = np.exp(-(geo.rface_norm()-inpars.nbi_deposition_location)**2/(2*inpars.nbi_particle_width**2)) # calculate nbi particle deposition profile (facegrid)
    C = inpars.S_nbi_tot / np.trapz(geo.vpr_face()*S_nbi_face, geo.rface()) # calculate constant prefactor

    nbi_source = CellVariable(mesh=geo.mesh, value=C*S_nbi) / inpars.nref

    return puff_source, pellet_source, nbi_source


def calc_neoclassical(inpars, geo, consts, profiles):
    '''
    Neoclassical conductivity and bootstrap current.
    Formulas from Sauter PoP 1999. Future work can include Redl PoP 2021 corrections.
    Should be compared later to RAPTOR with Zeff=1.
    Calculation done on face grid. Conducitivty output is interpolated to cell grid.
    Bootstrap current outputted both on face and cell grids
    '''

    # trapped fraction
    epsilon = (geo.Rout_face - geo.Rin_face)/(geo.Rout_face + geo.Rin_face) # local r/R
    epseff = 0.67 * (1. - 1.4*np.abs(geo.delta_face)*geo.delta_face)*epsilon
    aa=(1. - epsilon)/(1. + epsilon)
    ftrap = 1. - np.sqrt(aa)*(1. - epseff)/(1. + 2.*np.sqrt(epseff))

    # Spitzer conductivity
    NZ = 0.58 + 0.74 / (0.76 + inpars.Zeff)
    lnLame = 31.3 - np.log(np.sqrt(profiles.ne.faceValue()*inpars.nref)/(profiles.Te.faceValue()*1e3))  # note for later: ne
    lnLami = 30 - np.log(inpars.Zi**3*np.sqrt(profiles.ne.faceValue()*inpars.nref)/( (profiles.Ti.faceValue()*1e3)**1.5)) # note for later: ni
    #
    epsilon0 = 8.854e-12
    keV2J = 1e3*1.6e-19
    qe = 1.6e-19
    me = 9.11e-31

    lam_ei = 15.2 - 0.5*np.log(profiles.ne.faceValue()/1e20*inpars.nref) + np.log(profiles.Te.faceValue())
    tau_e = 12*np.pi**1.5/qe**4*(me/2)**0.5*epsilon0**2 * (profiles.Te.faceValue()*keV2J)**1.5/(profiles.ne.faceValue()*inpars.nref*lam_ei)
    #
    sigsptz = 1.9012e+04*(profiles.Te.faceValue()*1e3)**1.5/inpars.Zeff/NZ/lnLame

    nuestar = 6.921E-18*geo.q*inpars.Rmaj*profiles.ne.faceValue()*inpars.nref*inpars.Zeff* lnLame/(((profiles.Te.faceValue()*1e3)**2)* (epsilon+consts.eps)**1.5)
    nuistar = 4.9E-18*geo.q*inpars.Rmaj*profiles.ni.faceValue()*inpars.nref*inpars.Zeff**4* lnLami/(((profiles.Ti.faceValue()*1e3)**2)* (epsilon+consts.eps)**1.5)

    # Neoclassical correction to spitzer conductivity
    ft33 = ftrap/(1. + (0.55-0.1*ftrap)*np.sqrt(nuestar) + 0.45*(1.-ftrap)*nuestar/(inpars.Zeff**1.5))
    signeo = (1. - ft33*(1.+0.36/inpars.Zeff - ft33*(0.59/inpars.Zeff - 0.23/inpars.Zeff*ft33)))
    sigmaneo_face = sigsptz * signeo

    # Calculate terms needed for bootstrap current
    ft31 = ftrap/(1. + (1-0.1*ftrap)*np.sqrt(nuestar) + 0.5*(1.-ftrap)*nuestar/inpars.Zeff)
    ft32ee = ftrap/(1 + 0.26*(1-ftrap)*np.sqrt(nuestar) + 0.18*(1 - 0.37*ftrap)*nuestar/np.sqrt(inpars.Zeff))
    ft32ei = ftrap/(1 + (1 + 0.6*ftrap)*np.sqrt(nuestar) + 0.85*(1 - 0.37*ftrap)*nuestar*(1 + inpars.Zeff))
    ft34 = ftrap/(1. + (1-0.1*ftrap)*np.sqrt(nuestar) + 0.5*(1.-0.5*ftrap)*nuestar/inpars.Zeff)

    F32ee = (0.05 + 0.62*inpars.Zeff)/(inpars.Zeff*(1+0.44*inpars.Zeff))*(ft32ee - ft32ee**4) + \
     1/(1 + 0.22*inpars.Zeff)*(ft32ee**2 - ft32ee**4 - 1.2*(ft32ee**3 - ft32ee**4)) + \
     1.2/(1 + 0.5*inpars.Zeff)*ft32ee**4

    F32ei = -(0.56 + 1.93*inpars.Zeff)/(inpars.Zeff*(1+0.44*inpars.Zeff))*(ft32ei - ft32ei**4) + \
            4.95/(1 + 2.48*inpars.Zeff)*(ft32ei**2 - ft32ei**4 - 0.55*(ft32ei**3 - ft32ei**4)) - \
            1.2/(1 + 0.5*inpars.Zeff)*ft32ei**4

    L31 = (1 + 1.4/(inpars.Zeff+1))*ft31 - 1.9/(inpars.Zeff+1)*ft31**2 + \
          0.3/(inpars.Zeff+1)*ft31**3 + 0.2/(inpars.Zeff+1)*ft31**4

    L32 = F32ee + F32ei

    L34 = (1 + 1.4/(inpars.Zeff+1))*ft34 - 1.9/(inpars.Zeff+1)*ft34**2 + \
          0.3/(inpars.Zeff+1)*ft34**3 + 0.2/(inpars.Zeff+1)*ft34**4

    alpha0 = -1.17*(1-ftrap)/(1 - 0.22*ftrap - 0.19*ftrap**2)
    alpha = (alpha0 + 0.25*(1 - ftrap**2)*np.sqrt(nuistar)/(1 + 0.5*np.sqrt(nuistar)) + 0.315*nuistar**2*ftrap**6)/ \
            (1 + 0.15*nuistar**2*ftrap**6)


    # calculate bootstrap current
    prefactor = - geo.F_face*inpars.bootstrap_mult*2*consts.pi/geo.B0
    pe = profiles.ne.faceValue()*(profiles.Te.faceValue())*1e3*1.6e-19*inpars.nref
    pi = profiles.ni.faceValue() * profiles.Ti.faceValue() * 1e3 * 1.6e-19*inpars.nref

    dpsi_drnorm = profiles.psi.faceGrad()[0]
    dlnne_drnorm = profiles.ne.faceGrad()[0] / profiles.ne.faceValue()
    dlnni_drnorm = profiles.ni.faceGrad()[0] / profiles.ni.faceValue()
    dlnte_drnorm = profiles.Te.faceGrad()[0] / profiles.Te.faceValue()
    dlnti_drnorm = profiles.Ti.faceGrad()[0] / profiles.Ti.faceValue()
    global_coeff = np.zeros(len(dpsi_drnorm))
    global_coeff[1:] = prefactor[1:] / dpsi_drnorm[1:]

    necoeff = L31 * pe
    nicoeff = L31 * pi
    tecoeff = (L31 + L32) * pe
    ticoeff = (L31 + alpha * L34) * pi

    j_bootstrap_face = global_coeff * (necoeff * dlnne_drnorm + nicoeff * dlnni_drnorm + tecoeff * dlnte_drnorm + ticoeff * dlnti_drnorm)
    geo.I_bootstrap = np.trapz(j_bootstrap_face*geo.spr_face, geo.rface())

    j_bootstrap = face_to_cell(inpars, j_bootstrap_face)
    sigmaneo = face_to_cell(inpars, sigmaneo_face)

    return sigmaneo, j_bootstrap, j_bootstrap_face

def coll_exchange(inpars, consts, profiles):
    lam_ei = 15.2 - 0.5*np.log(profiles.ne()/1e20*inpars.nref) + np.log(profiles.Te())
    tau_e = 12*consts.pi**1.5/consts.qe**4*(consts.me/2)**0.5*consts.epsilon0**2 * (profiles.Te()*consts.keV2J)**1.5/(profiles.ne()*lam_ei*inpars.nref)
    Qei_coef = 1.5*profiles.ne()*inpars.nref*consts.keV2J/ (inpars.Ai*consts.mp/(2*consts.me)*tau_e) * inpars.Qei_mult
    return Qei_coef

def calculate_fusion(inpars, geo, consts, profiles):
    T = profiles.Ti.faceValue()

    # P [W/m^3] = Efus *1/4 * n^2 * <sigma*v>.
    # <sigma*v> for DT calculated with the Bosch-Hale parameterization NF 1992. T is in keV for the formula

    Efus = 17.6*1e3*consts.keV2J
    mrc2 = 1124656
    BG = 34.3827
    C1 = 1.17302e-9
    C2 = 1.51361e-2
    C3 = 7.51886e-2
    C4 = 4.60643e-3
    C5 = 1.35e-2
    C6 = -1.0675e-4
    C7 = 1.366e-5

    theta = T/(1 - (T*(C2+T*(C4+T*C6)))/ (1+T*(C3+T*(C5+T*C7))) )
    xi = (BG**2/(4*theta))**(1/3)
    sigmav = C1*theta*np.sqrt(xi/(mrc2*T**3))*np.exp(-3*xi) /1e6 #units of m^3/s

    Pfus = Efus*0.25*(profiles.ni.faceValue()*inpars.nref)**2*sigmav # [W/m^3]

    Pfus_cell = 0.5*(Pfus[:-1]+Pfus[1:])

    Ptot = np.trapz(Pfus*geo.vpr_face(), geo.rface())/1e6 # [MW]

    alpha_fraction = 3.5/17.6 # fusion power fraction to alpha particles

    # fractional fusion power ions/electrons, from Mikkelsen Nucl. Tech. Fusion 237 4 1983
    C1 = 88./profiles.Te()
    C2 = np.sqrt(C1)
    frac_i = 2*(0.166667*np.log((1-C2+C1)/(1 + 2*C2+C1))+0.57735026*(np.arctan(0.57735026*(2*C2-1))+0.52359874))/C1
    frac_e = 1 - frac_i
    Pfus_i = CellVariable(mesh=geo.mesh, value=Pfus_cell*frac_i*alpha_fraction)
    Pfus_e = CellVariable(mesh=geo.mesh, value=Pfus_cell*frac_e*alpha_fraction)

    return Ptot, Pfus_i, Pfus_e

def internal_boundary(inpars, geo):
    # Create Boolean mask FiPy CellVariable with True where the internal boundary condition is
    idx = np.abs(geo.rnp_norm - inpars.Ped_top).argmin() # find index closest to pedestal top
    mask_np = np.zeros(len(geo.rnp),dtype=bool)
    if inpars.set_pedestal:
        mask_np[idx] = True
    mask = CellVariable(mesh=geo.mesh, value=mask_np) # all false if no internal boundary condition
    return mask

def calc_face_gradient(profile, r):
    """Calculates face-grid gradients for kinetic profiles for arbitrary radial coordinate.
    Assumes zero boundary condition on left side

    Args:
      profile: Input profile (e.g. T, n) on cell grid
      r: Input radial coordinate on cell grid

    Returns:
      dprofiledr: gradient on face grid
    """

    profile_np = profile() # obtain profile as numpy array
    boundary_condition = profile.faceValue()[-1]


    dprofiledr = np.zeros(len(profile_np)+1)
    for i in range(len(profile)-1):
        dprofiledr[i+1] = (profile_np[i+1]-profile_np[i])/(r[i+1]-r[i])
    
    dprofiledr[-1] = (boundary_condition - profile_np[-1])/(0.5*(r[-1]-r[-2])) # set right side gradient using face boundary condition and half-width dr

    return dprofiledr

def chiCGM(inpars, geo, consts, profiles):
    # ITG critical gradient model. R/LTi_crit from Guo Romanelli 1993
    # chi_i = chiGB * chistiff * H(R/LTi - R/LTi_crit)*(R/LTi - R/LTi_crit)^alpha

    s = geo.s.copy()
    q = geo.q.copy()

    s[np.where(q<1)] = 0 # very basic sawtooth model
    q[np.where(q<1)] = 1 # very basic sawtooth model

    # set critical gradient
    rlti_crit = 4/3*(1 + profiles.Ti.faceValue()/profiles.Te.faceValue())*(1 + 2*np.abs(s)/q)

    # gyrobohm diffusivity
    chiGB = (inpars.Ai*consts.mp)**0.5/(consts.qe*geo.B0)**2*(profiles.Ti.faceValue()*consts.keV2J)**1.5/inpars.Rmaj

    # R/LTi profile from current timestep Ti

    # define radial coordinate as midplane average r (typical assumption for transport models developed in circular geo)
    rmid = (geo.Rout - geo.Rin)*0.5

    # calculate exact face gradient. Higher order derivatives like np.gradient can lead to numerical artifacts
    dTidr = calc_face_gradient(profiles.Ti, rmid)
    rlti = -inpars.Rmaj*dTidr/profiles.Ti.faceValue()

    # set minimum chi for PDE stability
    chi_ion = FaceVariable(mesh=geo.mesh, value = inpars.chimin)

    # built CGM model ion heat transport coefficient
    chi_ion.setValue( chiGB*inpars.CGMchistiff*(rlti - rlti_crit)**inpars.CGMalpha , where=(rlti >= rlti_crit) )

    # set (high) ceiling to CGM flux for PDE stability (might not be necessary with Perezerev)
    chi_ion.setValue( inpars.chimax , where=(chi_ion > inpars.chimax) )

    if inpars.set_pedestal: # set low transport in pedestal region to facilitate PDE solver (more consistency between desired profile and transport coefficients)
        chi_ion.setValue( inpars.chimin, where=(geo.rface_norm >= inpars.Ped_top) )

    # set electron heat transport coefficient to user-defined ratio of ion heat transport coefficient
    chi_el = FaceVariable(mesh=geo.mesh, value = chi_ion/inpars.CGMchiei_ratio)
    D_el = FaceVariable(mesh=geo.mesh, value = chi_ion/inpars.CGM_D_ratio)
    V_el = FaceVariable(mesh=geo.mesh, value = 0) # no convection in this CGM model (not a realistic model for particle transport anyway)

    return chi_ion, chi_el, D_el, V_el

def chiqlknn(inpars, geo, consts, profiles):

    # gyrobohm diffusivity (defined here with Lref=Rmin due to QLKNN training set normalization)
    chiGB = (inpars.Ai*consts.mp)**0.5/(consts.qe*geo.B0)**2*(profiles.Ti.faceValue()*consts.keV2J)**1.5/inpars.Rmin

    # transport coefficients from the qlknn-hyper-10D model, K.L. van de Plassche PoP 2020

    # set up input vectors (all as numpy arrays on face grid)
    Zeff = inpars.Zeff*np.ones(len(geo.rface))

    # define radial coordinate as midplane average r (typical assumption for transport models developed in circular geo)
    rmid = (geo.Rout - geo.Rin)*0.5
    rmid_face = (geo.Rout_face - geo.Rin_face)*0.5

    # calculate exact face gradients. Higher order derivatives like np.gradient can lead to numerical artifacts
    # R/LTi profile from current timestep Ti
    dTidr = calc_face_gradient(profiles.Ti, rmid)
    Ati = -inpars.Rmaj*dTidr/profiles.Ti.faceValue()
    Ati[np.where(np.abs(Ati) < consts.eps)] = consts.eps # to avoid divisions by zero

    # R/LTe profile from current timestep Te
    dTedr = calc_face_gradient(profiles.Te, rmid)
    Ate = -inpars.Rmaj*dTedr/profiles.Te.faceValue()
    Ate[np.where(np.abs(Ate) < consts.eps)] = consts.eps # to avoid divisions by zero

    # R/Lne profile from current timestep n
    dnedr = calc_face_gradient(profiles.ne, rmid)
    An = -inpars.Rmaj*dnedr/profiles.ne.faceValue()
    An[np.where(np.abs(An) < consts.eps)] = consts.eps # to avoid divisions by zero

    # q and s
    q = geo.q.copy()
    smag = geo.s.copy()

    # local r/a. Include aspect ratio correction to obtain same trapped electron fraction as qlknn10d training set
    epsilon = rmid_face[-1]/inpars.Rmaj
    epsilonNN = 1/3 # hard-coded for QLKNN10D inverse aspect ratio
    x = rmid_face/rmid_face[-1]*epsilon/epsilonNN 

    # Ion to electron temperature ratio
    Ti_Te = profiles.Ti.faceValue()/profiles.Te.faceValue()
    Ti_Te_ITG = np.ones(len(profiles.Ti.faceValue()))

    # logarithm of normalized collisionality
    logNustar = np.log10(calc_Nustar(inpars, geo, consts, profiles))

    # calculate alpha for magnetic shear correction (see S. van Mulders NF 2021)
    alpha = 2/geo.B0**2*consts.mu0 *q**2 * (profiles.Te.faceValue()*consts.keV2J*(profiles.ne.faceValue()*inpars.nref)*(Ate+An) + \
         (profiles.ni.faceValue()*inpars.nref)*profiles.Ti.faceValue()*consts.keV2J*(Ati+An) )

    # modifications of s and q consistent with physical assumptions
    smag = smag - alpha/2 # to approximate impact of Shafranov shift. From van Mulders Nucl. Fusion 2021
    smag[np.where(q<1)] = 0.1 # very basic ad-hoc sawtooth model
    q[np.where(q<1)] = 1 # very basic ad-hoc sawtooth model

    if inpars.avoid_big_negative_s:
        smag[np.where( (smag-alpha) < -0.2 )] = alpha[np.where( (smag-alpha) < -0.2 )] - 0.2

    feature_scan = np.array([Zeff, Ati, Ate, An, q, smag, x, Ti_Te, logNustar])

    df_scan = pd.DataFrame(feature_scan.transpose(),columns=['Zeff','Ati','Ate','An','q','smag','x','Ti_Te','logNustar'])

    # load all the separate neural networks.
    if inpars.include_ITG:
        net_itgleading = QuaLiKizNDNN.from_json(inpars.model_path+'/efiitg_gb.json')  # load ITG Qi net
        qi_itg = net_itgleading.get_output(df_scan).clip(0).to_numpy() # propagate inputs through net. Clip negative output to zero

        net_itgqediv = QuaLiKizNDNN.from_json(inpars.model_path+'/efeitg_gb_div_efiitg_gb.json') # load ITG Qe/Qi net
        qe_itg = net_itgqediv.get_output(df_scan).to_numpy() * qi_itg #propagate inputs through net and multiply by Qi

        net_itgpfediv = QuaLiKizNDNN.from_json(inpars.model_path+'/pfeitg_gb_div_efiitg_gb.json') # load ITG pfe/Qi net
        pfe_itg = net_itgpfediv.get_output(df_scan).to_numpy() * qi_itg #propagate inputs through net and multiply by Qi
    else:
        qi_itg = np.zeros(len(df_scan))
        qe_itg = np.zeros(len(df_scan))
        pfe_itg = np.zeros(len(df_scan))

    if inpars.include_TEM:
        net_temleading = QuaLiKizNDNN.from_json(inpars.model_path+'/efetem_gb.json') # load TEM Qe net
        qe_tem = net_temleading.get_output(df_scan).clip(0).to_numpy() # propagate inputs through net. Clip negative output to zero

        net_temqidiv = QuaLiKizNDNN.from_json(inpars.model_path+'/efitem_gb_div_efetem_gb.json') # load TEM Qi/Qe net
        qi_tem = net_temqidiv.get_output(df_scan).to_numpy() * qe_tem # propagate inputs through net.

        net_tempfediv = QuaLiKizNDNN.from_json(inpars.model_path+'/pfetem_gb_div_efetem_gb.json') # load TEM pfe/Qe net
        pfe_tem = net_tempfediv.get_output(df_scan).to_numpy() * qe_tem # propagate inputs through net.

    else:
        qi_tem = np.zeros(len(df_scan))
        qe_tem = np.zeros(len(df_scan))

    if inpars.include_ETG:
        net_etgleading = QuaLiKizNDNN.from_json(inpars.model_path+'/efeetg_gb.json') # load ETG Qe net
        qe_etg = net_etgleading.get_output(df_scan).clip(0).to_numpy() # propagate inputs through net. Clip negative output to zero
    else:
        qe_etg = np.zeros(len(df_scan))

    # combine fluxes
    qi = qi_itg.squeeze()+qi_tem.squeeze()
    qe = qe_itg.squeeze()*inpars.ITG_flux_ratio_correction + qe_tem.squeeze() + qe_etg.squeeze()

    pfe = pfe_itg.squeeze() + pfe_tem.squeeze()

    pfe_SI = pfe * profiles.ne.faceValue() * chiGB /inpars.Rmin  # conversion to SI units (note that n is normalized here)

    # chi outputs in SI units. chi in GB units is Q[GB]/(a/LT) , Lref=Rmin in Q[GB]. max/min clipping included
    chi_ion_np = inpars.Rmaj/inpars.Rmin*qi/Ati*chiGB
    chi_el_np = inpars.Rmaj/inpars.Rmin*qe/Ate*chiGB

    chi_ion_np[np.where(chi_ion_np > inpars.chimax)] = inpars.chimax
    chi_el_np[np.where(chi_el_np > inpars.chimax)] = inpars.chimax
    chi_ion_np[np.where(chi_ion_np < inpars.chimin)] = inpars.chimin
    chi_el_np[np.where(chi_el_np < inpars.chimin)] = inpars.chimin

    # set minimum chi for PDE stability
    chi_ion = FaceVariable(mesh=geo.mesh, value = chi_ion_np)
    chi_el = FaceVariable(mesh=geo.mesh, value = chi_el_np)

    # initialize diffusivity and convection
    D_el_np = np.zeros(len(pfe))
    V_el_np = np.zeros(len(pfe))

    # Effective D / Effective V approach.
    #For small density gradients or up-gradient transport, set pure effective convection. Otherwise pure effective diffusion
    if inpars.DVeff is True:
        Deff = -pfe_SI / (profiles.ne.faceGrad()[0]*geo.g1_over_vpr2_face/geo.rmax + consts.eps)
        Veff = pfe_SI / (profiles.ne.faceValue()*geo.g0_over_vpr_face)
        D_el_np = Deff.copy()
        V_el_np = Veff.copy()
        Deff_mask = (((pfe >= 0) & (An >= 0)) | ((pfe < 0) & (An < 0))) & (abs(An) >= inpars.An_min)
        Veff_mask = np.invert(Deff_mask)
        D_el_np[np.where(Veff_mask)] = 0
        V_el_np[np.where(Deff_mask)] = 0

    # Scaled D approach. Scale electron diffusivity to electron heat conductivity (this has some physical motivations),
    # and set convection to then match total particle transport
    if inpars.DVeff is False:
        finite_flux_idx = np.where(np.abs(pfe_SI) > 0)
        D_el_np[finite_flux_idx] = chi_el_np[finite_flux_idx]
        V_el_np = (pfe_SI / profiles.ne.faceValue() - An*D_el_np/inpars.Rmaj*geo.g1_over_vpr2_face)/geo.g0_over_vpr_face

    D_el_np[np.where(D_el_np < inpars.Demin)] = inpars.Demin
    D_el = FaceVariable(mesh=geo.mesh, value = D_el_np)
    V_el = FaceVariable(mesh=geo.mesh, value = V_el_np)

    if inpars.set_pedestal: # set low transport in pedestal region to facilitate PDE solver (more consistency between desired profile and transport coefficients)
        chi_ion.setValue( inpars.chimin, where=(geo.rface_norm.value >= inpars.Ped_top) )
        chi_el.setValue( inpars.chimin, where=(geo.rface_norm.value >= inpars.Ped_top) )
        D_el.setValue( inpars.Demin, where=(geo.rface_norm.value >= inpars.Ped_top) )
        V_el.setValue(0, where=(geo.rface_norm.value >= inpars.Ped_top))

    return chi_ion,chi_el,D_el,V_el

def calc_Nustar(inpars, geo, consts, profiles):
    # Lambe(:) = 15.2_DBL - 0.5_DBL*LOG(0.1_DBL*Nex(:)) + LOG(Tex(:))  !Coulomb constant and collisionality. Wesson 2nd edition p661-663
    Lambde = 15.2 - 0.5 * np.log(0.1 * profiles.ne.faceValue()/1e20*inpars.nref) + np.log(profiles.Te.faceValue())
    # ion_electron collision formula
    nu_e = 1 / 1.09e-3 * inpars.Zeff * profiles.ne.faceValue()/1e19*inpars.nref * Lambde / (profiles.Te.faceValue()) ** 1.5 * inpars.coll_mult

    # calculate bounce time
    epsilon = geo.rface.value / inpars.Rmaj
    epsilon[np.where(epsilon < consts.eps)] = consts.eps # to avoid divisions by zero
    tau_bounce = geo.q * inpars.Rmaj / (epsilon ** 1.5 * np.sqrt(profiles.Te.faceValue()*consts.keV2J / consts.me))
    tau_bounce[0] = tau_bounce[1] # due to pathological on-axis epsilon=0 term

    # calculate normalized collisionality
    nustar = nu_e * tau_bounce

    return nustar # returns nustar on face grid


def initialize_figures(inpars, geo, profiles):

    fig = None
    lines = None

    if inpars.plotall:
        fig = plt.figure(figsize=(12,6))
        ax1 = fig.add_subplot(421)
        ax2 = fig.add_subplot(422)
        ax3 = fig.add_subplot(423)
        ax4 = fig.add_subplot(424)
        ax5 = fig.add_subplot(425)
        ax6 = fig.add_subplot(426)
        ax7 = fig.add_subplot(427)
        ax8 = fig.add_subplot(428)
    elif inpars.plotchi:
        fig = plt.figure(figsize=(5,5))
        ax1 = fig.add_subplot(111)
    lines=[]

    if ( (inpars.plotchi) or (inpars.plotall) ):
        line, = ax1.plot(geo.rface_norm(), np.zeros(len(geo.rface_norm())),'r',label=r'$\chi_i$') # for chii
        lines.append(line)
        line, = ax1.plot(geo.rface_norm(), np.zeros(len(geo.rface_norm())),'b',label=r'$\chi_e$') # for chie
        lines.append(line)
        ax1.set_ylim([0, inpars.chi_ylim])
        ax1.set_xlabel("Normalized radius")
        ax1.set_ylabel(r"Heat conductivity $[m^2/s]$")
        ax1.legend()

    if inpars.plotall:
        line, = ax2.plot(geo.rnp_norm, profiles.ne(),'r')
        lines.append(line)
        ax2.set_ylim([0, np.max(profiles.ne())*1.1])
        ax2.set_xlabel("Normalized radius")
        ax2.set_ylabel(r"Density $[10^{20}~m^{-3}]$")
        
        line, = ax3.plot(geo.rnp_norm, np.zeros(len(geo.rnp_norm)),'r',label='External term')
        lines.append(line)
        line, = ax3.plot(geo.rnp_norm, np.zeros(len(geo.rnp_norm)),'b',label='Fusion term')
        lines.append(line)
        line, = ax3.plot(geo.rnp_norm, np.zeros(len(geo.rnp_norm)),'g',label='Ion-electron heat exchange')
        lines.append(line)
        ax3.set_ylim([inpars.Qlowlim, inpars.Qhilim])
        ax3.set_xlabel("Normalized radius")
        ax3.set_ylabel(r"Ion heat source $[kW~m^{-3}]$")
        ax3.legend(fontsize=8)

        line, = ax4.plot(geo.rnp_norm, np.zeros(len(geo.rnp_norm)),'r',label='External term')
        lines.append(line)
        line, = ax4.plot(geo.rnp_norm, np.zeros(len(geo.rnp_norm)),'b',label='Fusion term')
        lines.append(line)
        line, = ax4.plot(geo.rnp_norm, np.zeros(len(geo.rnp_norm)),'g',label='Ion-electron heat exchange')
        lines.append(line)
        ax4.set_ylim([inpars.Qlowlim, inpars.Qhilim])
        ax4.set_xlabel("Normalized radius")
        ax4.set_ylabel(r"Electron heat source $[kW~m^{-3}]$")
        ax4.legend(fontsize=8)

        line, = ax5.plot(geo.rface_norm(), geo.q,'r')
        lines.append(line)
        ax5.set_ylim([0, np.max(geo.q)+0.2])
        ax5.set_xlabel("Normalized radius")
        ax5.set_ylabel("q-profile")

        line, = ax6.plot(geo.rface_norm(), geo.s,'r')
        lines.append(line)
        ax6.set_ylim([np.min(geo.s)-0.2, np.max(geo.s+0.5)])
        ax6.set_xlabel("Normalized radius")
        ax6.set_ylabel("Magnetic shear")

        line, = ax7.plot(geo.rface_norm(), geo.jtot_face/1e6,'r',label='Total current')
        lines.append(line)
        line, = ax7.plot(geo.rface_norm(), geo.jext_face/1e6,'b',label='External current')
        lines.append(line)
        line, = ax7.plot(geo.rface_norm(), geo.j_bootstrap_face/1e6,'g',label='Bootstrap current')
        lines.append(line)
        line, = ax7.plot(geo.rface_norm(), geo.johm_face/1e6,'m',label='Ohmic current')
        lines.append(line)
        ax7.set_ylim([0, np.max(geo.jtot/1e6)*1.1])
        ax7.set_xlabel("Normalized radius")
        ax7.set_ylabel(r"Current $[MA~m^{-2}]$")
        ax7.legend(fontsize=8)

        line, = ax8.plot(geo.rnp_norm, profiles.Ti(),'r',label=r'$T_i$')
        lines.append(line)
        line, = ax8.plot(geo.rnp_norm, profiles.Te(),'b',label=r'$T_e$')
        lines.append(line)
        Tmax = max([max(profiles.Ti()),max(profiles.Te())])
        ax8.set_ylim([0, Tmax*1.1])
        ax8.set_xlabel("Normalized radius")
        ax8.set_ylabel("Temperature [keV]")
        ax8.legend(fontsize=8)

        fig.tight_layout()

        return fig, lines, [ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8]
    return fig, lines, []

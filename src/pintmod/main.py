from fipy import Variable, FaceVariable, CellVariable, ExplicitDiffusionTerm, DiffusionTerm, TransientTerm, \
ImplicitSourceTerm, ConvectionTerm
from fipy.tools import numerix
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import yaml
import argparse
import h5py
import time
import dataclasses
from IPython import embed

from lib import *
from parameter_template import *

st = time.perf_counter()

# Instantiate input, constant, and geometry structures
inpars = Input()  # all inputs from parameter_template.py are now in inpars structure

# read config file from parser (optional)
parser = argparse.ArgumentParser(description='Set up integrated modelling run')
parser.add_argument("--config", nargs=1, help="Location of configuration file to overwrite defaults")
parser.add_argument("--outfile", nargs=1, help="Name of output h5 file")
args = parser.parse_args()

# replace attributes in inpars with custom configuration file
if args.config != None:
    with open(args.config[0]+'.yaml') as f:
        configs = yaml.load(f, Loader=yaml.FullLoader)
    for key in configs:
        if not hasattr(inpars, key):
            raise Exception(key+" not a valid attribute in configuration file")
        else:
            setattr(inpars, key, configs[key])


consts = Constants() # structure for physical constants
geo = Geometry(inpars, consts) #  instantiate structure for all the geometric quantities (e.g. the mesh)

# Initialize profiles
profiles, geo = init_profiles(inpars, geo, consts) # initialize Ti, Te, n, psi, magnetic geometry terms

if inpars.test_geometry and inpars.geometry_type == 'CHEASE':
    test_CHEASE_equilibrium(inpars, geo, consts, profiles)
    exit()

mask = internal_boundary(inpars, geo) # needed for internal boundary condition setup if inpars.set_pedestal == True

# Calculate constant heat sources
source_ion, source_el = calculate_source(inpars, geo)

# Calculate constant particle sources
puff_source, pellet_source, nbi_source = calculate_particle_sources(inpars, geo)

# initialize and configure temperature plot
if inpars.plot_T:
    T_fig = plt.figure(figsize=(5,5))
    T_ax = T_fig.add_subplot(111)

    T_ax.set_xlim([0, 1])
    T_ax.set_ylim([0, inpars.Tplot_ymax])
    T_ax.set_xlabel("Normalized radius")
    T_ax.set_ylabel("Temperature [keV]")

    line_Ti, = T_ax.plot(geo.rnp_norm, profiles.Ti(),'r',label=r'$T_i$')
    line_Te, = T_ax.plot(geo.rnp_norm, profiles.Te(),'b',label=r'$T_e$')
    T_ax.legend()

    plt.show(block=False)
    plt.draw()
    plt.pause(0.01)


# run simulation!
timenow = 0.0
psi_list = [] # lists for profiles to be later saved to disk (if requested)
j_list = []
johm_list = []
q_list = []
s_list = []
ti_list = []
te_list = []
ne_list = []
if inpars.implicit == False: # for explicit numpy calculation data write
    ti_explicit_list = []
    ti_explicit = profiles.Ti.value.copy() # initial condition

chii_list = []
chie_list = []

time_list = []

first_time = True

while timenow < inpars.finaltime:

    # geo structure only contains info on current timeslice
    geo.jtot_face, geo.jtot = calc_jtot_from_psi(inpars, geo, consts, profiles)
    geo.q, geo.q_cell = calc_q_from_psi(inpars, geo, consts, profiles)
    geo.s = calc_s_from_psi(inpars, geo, consts, profiles)

    if first_time:
        # initialize extra figures. Returns none if plot_chi and plot_all are both false
        fig, lines, axes = initialize_figures(inpars, geo, profiles)

    # calculate neoclassical conductivity and bootstrap current
    sigma_neo, j_bootstrap, j_bootstrap_face = calc_neoclassical(inpars, geo, consts, profiles)
    geo.j_bootstrap = j_bootstrap
    geo.j_bootstrap_face = j_bootstrap_face

    # calculate ohmic current
    geo.johm = geo.jtot - geo.j_bootstrap - geo.jext
    geo.johm_face = geo.jtot_face - geo.j_bootstrap_face - geo.jext_face

    sigma = CellVariable(mesh=geo.mesh, value=sigma_neo) # set conductivity
    psi_dif_term = FaceVariable(mesh=geo.mesh, value = geo.G2_face/geo.J_face/geo.rmax**2)

    # set the transport coefficients in the transport equations, as fipy FaceVariables
    if (inpars.transport_model == 'qlknn'): # QuaLiKiz neural network model (qlknn-hyper)
        chii_ineq, chie_ineq, De_ineq, Ve_ineq = chiqlknn(inpars, geo, consts, profiles) # obtain transport coefficients from the qlknn-hyper model
    elif (inpars.transport_model == 'CGM'): # CGM model
        chii_ineq, chie_ineq, De_ineq, Ve_ineq = chiCGM(inpars, geo, consts, profiles) # obtain transport coefficients from CGM model
    elif (inpars.transport_model == 'constant'): # constant chi model
        chii_ineq = FaceVariable(mesh=geo.mesh, value = inpars.chii)
        chie_ineq = FaceVariable(mesh=geo.mesh, value = inpars.chie)
        De_ineq = FaceVariable(mesh=geo.mesh, value = inpars.De)
        Ve_ineq = FaceVariable(mesh=geo.mesh, value = inpars.Ve)       
        if inpars.set_pedestal: # set particle convection to 0 in pedestal region
            Ve_ineq.setValue(0, where=(geo.rface_norm.value > inpars.Ped_top))

    # set inner core transport coefficient patches (ad-hoc MHD/EM)
    if inpars.apply_inner_patch:
        chii_ineq.setValue(inpars.chii_inner, where=(geo.rface_norm.value < inpars.rho_inner))
        chie_ineq.setValue(inpars.chie_inner, where=(geo.rface_norm.value < inpars.rho_inner))
        De_ineq.setValue(inpars.De_inner, where=(geo.rface_norm.value < inpars.rho_inner))
        Ve_ineq.setValue(inpars.Ve_inner, where=(geo.rface_norm.value < inpars.rho_inner))

    # set outer core transport coefficient patches (e.g. L-mode)
    if inpars.apply_outer_patch:
        chii_ineq.setValue(inpars.chii_outer, where=(geo.rface_norm.value > inpars.rho_outer))
        chie_ineq.setValue(inpars.chie_outer, where=(geo.rface_norm.value > inpars.rho_outer))
        De_ineq.setValue(inpars.De_outer, where=(geo.rface_norm.value > inpars.rho_outer))
        Ve_ineq.setValue(inpars.Ve_outer, where=(geo.rface_norm.value > inpars.rho_outer))

	# set terms as they appear in diffusion component of transport equations
    full_chii_term = chii_ineq*geo.g1_over_vpr_face*profiles.ni.faceValue()*inpars.nref*consts.keV2J/geo.rmax**2 # helper variables as fipy objects for insertion into PDE
    full_chie_term = chie_ineq*geo.g1_over_vpr_face*profiles.ne.faceValue()*inpars.nref*consts.keV2J/geo.rmax**2
    full_De_term = De_ineq*geo.g1_over_vpr_face/geo.rmax**2
    full_Ve_term = FaceVariable(mesh=geo.mesh, value = (Ve_ineq.value*geo.g0_face/geo.rmax,))  # convection inputs need to be a vector (this is badly documented in FiPy)

    Qei_coef = CellVariable(mesh=geo.mesh, value=coll_exchange(inpars, consts, profiles)) # set ion-electron heat exchange term
    if inpars.calculate_Pfus:  # calculate fusion power and sources
        Pfustot, Pfus_i, Pfus_e = calculate_fusion(inpars, geo, consts, profiles)

    # append output data
    time_list.append(timenow)
    psi_list.append(profiles.psi.value.copy())
    j_list.append(geo.jtot.copy())
    johm_list.append(geo.johm.copy())
    q_list.append(geo.q.copy())
    s_list.append(geo.s.copy())
    ti_list.append(profiles.Ti.value.copy())
    te_list.append(profiles.Te.value.copy())
    ne_list.append(profiles.ne.value.copy())
    if inpars.implicit == False:
        ti_explicit_list.append(ti_explicit.copy())
    chii_list.append(chii_ineq.copy())
    chie_list.append(chie_ineq.copy())

    if (inpars.use_pereverzev_in_heat and inpars.implicit): # calculate terms if pereverzev-corrigan method is applied for heat transport (deals with stiff nonlinearity of transport coefficients)

        # set Perzerev diffusivities based on existing chi values and multiplier from parameters
        chiper_ion = full_chii_term/chii_ineq.value*inpars.chi_per
        chiper_el = full_chie_term/chie_ineq.value*inpars.chi_per

        # remove Pereverzev flux from boundary region if pedestal model is on (for PDE stability)
        chiper_ion_np = chiper_ion.value
        chiper_el_np = chiper_el.value
        if inpars.set_pedestal:
            chiper_ion_np[np.where(geo.rface_norm.value>inpars.Ped_top)] = 0
            chiper_el_np[np.where(geo.rface_norm.value>inpars.Ped_top)] = 0

        # remove Pereverzev flux from outer patch since numerically unstable with FiPy
        if inpars.apply_outer_patch:
            chiper_ion_np[np.where(geo.rface_norm.value>inpars.rho_outer)] = 0
            chiper_el_np[np.where(geo.rface_norm.value>inpars.rho_outer)] = 0
        chiper_ion_np[0] = chiper_ion_np[1] # removes a division by zero in the code. Doesn't impact results
        chiper_el_np[0] = chiper_el_np[1]

        # set up Pereverzev diffusion term as it will appear in the PDE
        full_chiiper_term = FaceVariable(mesh=geo.mesh, value=chiper_ion_np)
        full_chieper_term = FaceVariable(mesh=geo.mesh, value=chiper_el_np)

        # set Pereverzev convection term to have zero absolute flux
        Vper_ion = profiles.Ti.faceGrad()[0]/profiles.Ti.faceValue()*full_chiiper_term
        Vper_el = profiles.Te.faceGrad()[0]/profiles.Te.faceValue()*full_chieper_term

        # set convection term to correct FiPy type
        Vper_ion_ineq = FaceVariable(mesh=geo.mesh, value = (Vper_ion,))  # convection inputs need to be a vector (this is badly documented in FiPy)
        Vper_el_ineq = FaceVariable(mesh=geo.mesh, value = (Vper_el,))

    if (inpars.use_pereverzev_in_dens and inpars.implicit): # calculate terms if pereverzev-corrigan method is applied for density transport (deals with stiff nonlinearity of transport coefficients)

        # set Perzerev diffusivity array
        Dper_el_np = np.ones(len(full_De_term.value))*inpars.D_per/geo.rmax

        # remove Pereverzev flux from boundary region if pedestal model is on (for PDE stability)
        if inpars.set_pedestal:
            Dper_el_np[np.where(geo.rface_norm.value>inpars.Ped_top)] = 0
        # remove Pereverzev flux from outer patch region since numerically unstable with FiPy
        if inpars.apply_outer_patch:
            Dper_el_np[np.where(geo.rface_norm.value>inpars.rho_outer)] = 0            

        # set Pereverzev convection term to have zero absolute flux
        geo_factor = np.ones(len(geo.g0_face))
        geo_factor[1:] = geo.g1_over_vpr_face.value[1:] / geo.g0_face[1:]
        Vper_el_np = profiles.ne.faceGrad()[0]/profiles.ne.faceValue()*Dper_el_np*geo_factor

        # set terms to correct FiPy type
        full_De_dens_PC_term = FaceVariable(mesh=geo.mesh, value = Dper_el_np*geo.g1_over_vpr_face.value/geo.rmax)
        full_Ve_dens_PC_term = FaceVariable(mesh=geo.mesh, value = (Vper_el_np*geo.g0_face/geo.rmax,))  # convection inputs need to be a vector (this is badly documented in FiPy)

    psi_LHS = TransientTerm(coeff=1/inpars.resistivity_mult*geo.rcell*sigma*consts.mu0/geo.J**2/inpars.Rmaj, var=profiles.psi)   # LHS terms in current diffusion
    ion_heat_LHS = TransientTerm(coeff=1.5*geo.vpr*profiles.ni()*inpars.nref*consts.keV2J, var=profiles.Ti)  # LHS terms in ion pressure equation
    el_heat_LHS = TransientTerm(coeff=1.5*geo.vpr*profiles.ne()*inpars.nref*consts.keV2J, var=profiles.Te)   # LHS terms in electron pressure equation
    el_dens_LHS = TransientTerm(coeff=geo.vpr, var=profiles.ne)   # LHS terms in electron density equation
    psi_RHS = [] # list for building up terms in psi pressure equation
    ion_heat_RHS = [] # list for building up terms in ion pressure equation
    el_heat_RHS = [] # list for building up terms in el pressure equation
    el_dens_RHS = [] # list for building up terms in el density equation

    psi_RHS.append(CellVariable(mesh=geo.mesh, value=-geo.vpr*(geo.jext+j_bootstrap)*consts.mu0/(2*consts.pi*inpars.Rmaj*geo.J**2))) # psi source term
    if inpars.implicit:
        psi_RHS.append(DiffusionTerm(coeff=psi_dif_term, var=profiles.psi)) # psi diffusion term
        ion_heat_RHS.append(DiffusionTerm(coeff=full_chii_term, var=profiles.Ti)) # ion heat diffusion term
        ion_heat_RHS.append(source_ion*geo.vpr + \
            ImplicitSourceTerm(Qei_coef*geo.vpr, var=profiles.Te) - ImplicitSourceTerm(Qei_coef*geo.vpr, var=profiles.Ti)) #ion heat source term
        if inpars.use_fusion_heat_source: #include self-consistent fusion heat source
            ion_heat_RHS.append(Pfus_i*geo.vpr)
        el_heat_RHS.append(DiffusionTerm(coeff=full_chie_term, var=profiles.Te)) # el heat diffusion term
        el_heat_RHS.append(source_el*geo.vpr - \
            ImplicitSourceTerm(Qei_coef*geo.vpr, var=profiles.Te) + ImplicitSourceTerm(Qei_coef*geo.vpr, var=profiles.Ti)) #el heat source term
        if inpars.use_fusion_heat_source: #include self-consistent fusion heat source
            el_heat_RHS.append(Pfus_e*geo.vpr)
        el_dens_RHS.append(DiffusionTerm(coeff=full_De_term, var=profiles.ne) - ConvectionTerm(coeff=full_Ve_term, var=profiles.ne)) # el particle flux term
        el_dens_RHS.append((puff_source + pellet_source + nbi_source)*geo.vpr)

        if inpars.set_pedestal: # adaptive source for internal boundary condition
            ion_heat_RHS.append(mask*inpars.largeValue_T*inpars.Tiped - ImplicitSourceTerm(mask*inpars.largeValue_T, var=profiles.Ti)) # ion adaptive source
            el_heat_RHS.append(mask*inpars.largeValue_T*inpars.Teped - ImplicitSourceTerm(mask*inpars.largeValue_T, var=profiles.Te)) # el adaptive source
            el_dens_RHS.append(mask*inpars.largeValue_n*inpars.neped - ImplicitSourceTerm(mask*inpars.largeValue_n, var=profiles.ne)) # el adaptive source

        if inpars.use_pereverzev_in_heat: # add Pereverzev-Corrigan zero flux term to deal with nonlinearity in heat equation
            ion_heat_RHS.append(DiffusionTerm(coeff=full_chiiper_term, var=profiles.Ti) - ConvectionTerm(coeff=Vper_ion_ineq, var=profiles.Ti))
            el_heat_RHS.append(DiffusionTerm(coeff=full_chieper_term, var=profiles.Te) - ConvectionTerm(coeff=Vper_el_ineq, var=profiles.Te))

        if inpars.use_pereverzev_in_dens: # add Pereverzev-Corrigan zero flux term to deal with nonlinearity in density equation
            el_dens_RHS.append(DiffusionTerm(coeff=full_De_dens_PC_term, var=profiles.ne) - ConvectionTerm(coeff=full_Ve_dens_PC_term, var=profiles.ne))

    else:
        psi_RHS.append(ExplicitDiffusionTerm(coeff=psi_dif_term, var=profiles.psi)) # psi diffusion term
        ion_heat_RHS.append(ExplicitDiffusionTerm(coeff=full_chii_term, var=profiles.Ti)) # ion diffusion term
        ion_heat_RHS.append(source_ion*geo.vpr + Qei_coef*geo.vpr*(profiles.Te-profiles.Ti)) # ion source term
        el_heat_RHS.append(ExplicitDiffusionTerm(coeff=full_chie_term, var=profiles.Te))  # el diffusion term
        el_heat_RHS.append(source_el*geo.vpr - Qei_coef*geo.vpr*(profiles.Te - profiles.Ti)) # el source term
        if inpars.set_pedestal: # adaptive source for internal boundary condition
            ion_heat_RHS.append(mask*inpars.largeValue_T*(inpars.Tiped - profiles.Ti)) # ion adaptive source
            el_heat_RHS.append(mask*inpars.largeValue_T*(inpars.Teped - profiles.Te) ) # el adaptive source

        # calculate explicit method also by hand to compare (not all these coefficients need to be calculated every timestep but done so for clarity)
        # only valid here for test1 (no ion-electron heat exchange, only constant chi, Ti only)
        # all done in numpy, no fipy. All done on cell grid, with a ghost cell formulation for the outer boundary condition

        ct = 1.5*geo.vpr()*profiles.ne.value*inpars.nref*consts.keV2J # transient term coefficient vector (has radial dependence through r, n)
        cD = geo.g1_over_vpr()*profiles.ne.value*inpars.nref*consts.keV2J*inpars.chii/geo.rmax**2 #diffusion term coefficient
        P = source_ion.value*geo.vpr() # source term

        T_BC = inpars.T_bound_right # temperature right boundary condition (zero gradient left boundary condition directly incorporated in A matrix)
        cD_BC = geo.g1_over_vpr_face()[-1]*profiles.ni.faceValue()[-1]*inpars.nref*consts.keV2J*inpars.chii/geo.rmax**2  # diffusion term coefficient at last face cell

        # construction of the A discretization matrix (tridiagonal)

        a = (np.roll(cD, -1)[:-1] + cD[:-1])/(2*geo.dr_norm**2)
        b = -(np.roll(cD, -1) + 2*cD + np.roll(cD, 1))/(2*geo.dr_norm**2)

        # set boundary rows
        b[0] = -a[0] # for zero gradient boundary condition
        b[-1] = -1/(2*geo.dr_norm**2)*(cD[-2] + cD[-1] + 4*cD_BC)  # needed due to ghost cell which depends on face cell boundary linear extrapolation

        A = np.diag(a,1) + np.diag(a,-1) + np.diag(b)

        # boundary condition vector
        vec_BC = np.zeros(inpars.nr)
        vec_BC[-1] = 2*T_BC*cD_BC/geo.dr_norm**2

        # dt at 0.9 of stability limit
        dt = 0.9 * geo.dr_norm**2 / (2 * max(inpars.chii*geo.g1_over_vpr2_face)/geo.rmax**2 ) *1.5
        ti_explicit = ti_explicit + dt*(np.matmul(A,ti_explicit) + P + vec_BC)/ct

    eq_psi = psi_LHS == sum(psi_RHS)
    eq_ion_heat = ion_heat_LHS == sum(ion_heat_RHS)
    eq_el_heat = el_heat_LHS == sum(el_heat_RHS)
    eq_el_dens = el_dens_LHS == sum(el_dens_RHS)

    if not inpars.implicit:
            eq = eq_ion_heat # explicit solver fails for coupled equations. Only used for testing purposes, for ion heat equation only
            if first_time:
                print('Explicit solver: simulating ion heat transport only')
                first_time = False
    else:
        if inpars.current_eq and inpars.heat_eq and inpars.dens_eq:
            eq = eq_psi & eq_ion_heat & eq_el_heat & eq_el_dens
            if first_time:
                print('Simulating current diffusion, heat transport, and particle transport')
                first_time = False
        if not inpars.current_eq and inpars.heat_eq and inpars.dens_eq:
            eq = eq_ion_heat & eq_el_heat & eq_el_dens
            if first_time:
                print('Simulating heat transport and particle transport')
                first_time = False
        if inpars.current_eq and inpars.heat_eq and not inpars.dens_eq:
            eq = eq_psi & eq_ion_heat & eq_el_heat
            if first_time:
                print('Simulating current diffusion and heat transport')
                first_time = False
        if not inpars.current_eq and inpars.heat_eq and not inpars.dens_eq:
            eq = eq_ion_heat & eq_el_heat
            if first_time:
                print('Simulating heat transport only')
                first_time = False
        if inpars.current_eq and not inpars.heat_eq and not inpars.dens_eq:
            eq = eq_psi
            if first_time:
                print('Simulating current diffusion only')
                first_time = False

    # "basic" dt for diffusion equation, set as the explicit stability limit
    basic_dt = geo.dr_norm**2 / (2 * max(np.concatenate([chii_ineq.value*geo.g1_over_vpr2_face, chie_ineq.value*geo.g1_over_vpr2_face])) ) *1.5 * geo.rmax**2  # likely safe to not include De in max, but should be checked later

    # multiply basic timestep by multiplier to get desired actual timestep
    timeStepDuration = min(basic_dt*inpars.dtmult,inpars.maxdt)

    # output time and timestep info onto temperature plot
    if inpars.plot_T:
        timedata = "t = "+str(np.round(timenow,4))+"s"+". dt = "+str(np.round(timeStepDuration,6))
        if 'time_text' in locals():
            time_text.remove() # remove previous text if exists, to keep clean
        time_text = T_ax.text(0.01,inpars.Tplot_ymax*0.01,timedata)

        # output fusion power info onto temperature plot
        if inpars.show_Pfus and inpars.calculate_Pfus:
            if 'fusion_text' in locals():
                fusion_text.remove() # remove previous text if exists, to keep clean
            fusion_text = T_ax.text(0.01,inpars.Tplot_ymax*0.07,r'$P_{fus}=$'+str(np.round(Pfustot,2))+' MW')

        # output Greenwald fraction info onto temperature plot
        if inpars.show_fGW:
            if 'fGWtext' in locals():
                fGWtext.remove() # remove previous text if exists, to keep clean
            nbar = np.trapz(profiles.ne(), geo.Rout)/geo.Rmin_out
            fGW = nbar*inpars.nref/(inpars.Ip/ ( np.pi * inpars.Rmin**2) * 1e20)
            fGWtext = T_ax.text(0.01,inpars.Tplot_ymax*0.13,r'$f_{GW}=$'+str(np.round(fGW,3)))


        # find lines for current timestep
        line_Te.set_ydata(profiles.Te())
        line_Ti.set_ydata(profiles.Ti())

        T_fig.canvas.draw()
        plt.pause(0.01)

    if inpars.write_dt:
        print("t = "+str(np.round(timenow,5))+"s"+". dt = "+str(np.round(timeStepDuration,5)))

    # plot extra variables if desired
    Qei = Qei_coef()*(profiles.Te() - profiles.Ti()) # as numpy array
    if inpars.plotall:
        datalist = [chii_ineq.value, chie_ineq.value, profiles.ne(), source_ion()/1e3, Pfus_i()/1e3, Qei/1e3, source_el()/1e3, Pfus_e()/1e3, -Qei/1e3, geo.q, geo.s, \
        geo.jtot_face/1e6, geo.jext_face/1e6, geo.j_bootstrap_face/1e6, geo.johm_face/1e6, profiles.Ti(), profiles.Te()]
        for line, data in zip(lines, datalist):
            line.set_ydata(data)
        fbs = geo.I_bootstrap/1e6/inpars.Ip

        Qi_lowlim = min([min(source_ion()/1e3), min(Pfus_i()/1e3), min(Qei/1e3)])
        Qe_lowlim = min([min(source_el()/1e3), min(Pfus_e()/1e3), min(-Qei/1e3)])
        Qi_hilim = max([max(source_ion()/1e3), max(Pfus_i()/1e3), max(Qei/1e3)])
        Qe_hilim = max([max(source_el()/1e3), max(Pfus_e()/1e3), max(-Qei/1e3)])
        if Qi_lowlim < 0:
            axes[2].set_ylim([Qi_lowlim*1.1, Qi_hilim*1.1])
        else:
            axes[2].set_ylim([Qi_lowlim/1.1, Qi_hilim*1.1])
        if Qe_lowlim < 0:
            axes[3].set_ylim([Qe_lowlim*1.1, Qe_hilim*1.1])
        else:
            axes[3].set_ylim([Qe_lowlim/1.1, Qe_hilim*1.1])

        # output fusion power info onto temperature plot
        Tmax = max([max(profiles.Ti()),max(profiles.Te())])
        axes[7].set_ylim([0, Tmax*1.1])
        if inpars.show_Pfus and inpars.calculate_Pfus:
            if 'fusion_text_allplots' in locals():
                fusion_text_allplots.remove() # remove previous text if exists, to keep clean
            fusion_text_allplots = axes[7].text(0.01,Tmax*0.07,r'$P_{fus}=$'+str(np.round(Pfustot,2))+' MW')

        # output Greenwald fraction info onto temperature plot
        axes[1].set_ylim([0, np.max(profiles.ne())*1.1])
        if inpars.show_fGW:
            if 'fGW_text_allplots' in locals():
                fGW_text_allplots.remove() # remove previous text if exists, to keep clean
            nbar = np.trapz(profiles.ne(), geo.Rout)/geo.Rmin_out
            fGW = nbar*inpars.nref/(inpars.Ip/ ( np.pi * inpars.Rmin**2) * 1e20)
            fGW_text_allplots = axes[1].text(0.01,np.max(profiles.ne())*1.1*0.05,r'$f_{GW}=$'+str(np.round(fGW,3)))

        if 'fbs_text' in locals():
            fbs_text.remove()
        axes[6].set_ylim([0, np.max(geo.jtot/1e6)*1.1])
        fbs_text = axes[6].text(0.02,np.max(geo.jtot/1e6)*0.05,r'$f_{bs}=$'+str(np.round(fbs,2)),fontsize=10)

        fig.canvas.draw()
        plt.pause(0.01)
    elif inpars.plotchi:
        datalist = [chii_ineq.value, chie_ineq.value]
        for line, data in zip(lines, datalist):
            line.set_ydata(data)
        fig.canvas.draw()
        plt.pause(0.01)

    # solve for next timestep
    eq.solve(dt=timeStepDuration)
    #embed()

    # update ion profile
    dilution_factor = (inpars.Zimp - inpars.Zeff)/(inpars.Zimp - 1)
    profiles.ni.setValue(profiles.ne()*dilution_factor)

    # update time
    timenow = timenow + timeStepDuration

# append output data

geo.jtot_face, geo.jtot = calc_jtot_from_psi(inpars, geo, consts, profiles)
geo.q, geo.q_cell = calc_q_from_psi(inpars, geo, consts, profiles)
geo.s = calc_s_from_psi(inpars, geo, consts, profiles)

chii_list.append(chii_ineq.copy())
chie_list.append(chie_ineq.copy())
psi_list.append(profiles.psi.value)
j_list.append(geo.jtot.copy())
johm_list.append(geo.johm.copy())
q_list.append(geo.q.copy())
s_list.append(geo.s.copy())
ti_list.append(profiles.Ti.value.copy())
te_list.append(profiles.Te.value.copy())
ne_list.append(profiles.ne.value.copy())

if inpars.implicit == False:
    ti_explicit_list.append(ti_explicit)
    Ti_explicit = np.array(ti_explicit_list)

time_list.append(timenow)

t = np.array(time_list)
psi = np.array(psi_list)
j = np.array(j_list)
johm = np.array(johm_list)
q = np.array(q_list)
s = np.array(s_list)
Ti = np.array(ti_list)
Te = np.array(te_list)
ne = np.array(ne_list)
chii = np.array(chii_list)
chie = np.array(chie_list)

if args.outfile != None:
    with h5py.File(args.outfile[0]+'.h5', 'w') as hf:
        hf.create_dataset('t',  data=t)
        hf.create_dataset('rcell',  data=geo.rnp)
        hf.create_dataset('rface',  data=geo.rface())
        hf.create_dataset('rcell_norm',  data=geo.rnp_norm)
        hf.create_dataset('rface_norm',  data=geo.rface_norm())
        hf.create_dataset('psi',  data=psi)
        hf.create_dataset('j',  data=j)
        hf.create_dataset('johm',  data=johm)
        hf.create_dataset('q',  data=q)
        hf.create_dataset('s',  data=s)
        hf.create_dataset('Ti',  data=Ti)
        hf.create_dataset('Te',  data=Te)
        hf.create_dataset('ne',  data=ne)
        hf.create_dataset('chii',  data=chii)
        hf.create_dataset('chie',  data=chie)
        if inpars.implicit == False:
            hf.create_dataset('Ti_explicit',  data=Ti_explicit)

et = time.perf_counter()
elapsed_time = et - st
print('CPU execution time:', np.round(elapsed_time, 3), 'seconds')

if inpars.plot_T or inpars.plotchi or inpars.plotall:
    input("Final state. Press <return> to proceed...") # end of simulation


